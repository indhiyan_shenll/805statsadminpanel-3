<?php 
include_once('session_check.php');
include_once('connect.php'); 


if((isset($_POST['post_type'])) && ($_POST['post_type']=='seasonorderchange')){	
	$seasonidArr  = $_POST['seasonid'];	
    $Inc = 1;

	foreach($seasonidArr as $seasonid){
		echo "UPDATE customer_season SET season_order=$Inc WHERE custid=$customerid and id=$seasonid ";

		$updateRes = $conn->prepare("UPDATE customer_season SET season_order=:season_order WHERE custid=:cid and id=:season_id ");
		$QryCond   = array(":season_order"=>$Inc,':cid'=>$customerid, ':season_id'=>$seasonid);
		$updateRes->execute($QryCond);
		
		$Inc++;
	}	
	echo "success";
	exit;
}

if((isset($_POST['post_type'])) && ($_POST['post_type']=='conforderchange')){	
	$seasonid         = $_POST['seasonid'];
	$conferenceidArr  = $_POST['conferenceid'];	
    $Inc = 1;

	foreach($conferenceidArr as $conferenceid){
	
		$updateRes = $conn->prepare("UPDATE customer_season_conference SET conf_order=:conf_order WHERE customer_id=:cid and season_id=:season_id and conference_id=:conference_id");
		$QryCond   = array(":conf_order"=>$Inc, ':season_id'=>$seasonid,':conference_id'=>$conferenceid,':cid'=>$customerid);
		$updateRes->execute($QryCond);
		
		$Inc++;
	}	
	echo "success";
	exit;
}

if((isset($_POST['post_type'])) && ($_POST['post_type']=='divisionorderchange')){
	$seasonid      = $_POST['seasonid'];
	$conferenceid  = $_POST['conferenceid'];
	$divisionidArr = $_POST['divisionid'];
	
	$Inc = 1;

	foreach($divisionidArr as $divisionid){
	
		$updateRes = $conn->prepare("UPDATE customer_conference_division SET div_order=:div_order WHERE customer_id=:cid and season_id=:season_id and division_id=:division_id and conference_id=:conference_id");
		$QryCond   = array(":div_order"=>$Inc, ':season_id'=>$seasonid, ':division_id'=>$divisionid, ':conference_id'=>$conferenceid,':cid'=>$customerid);
		$updateRes->execute($QryCond);
		
		$Inc++;
	}	
	echo "success";
	exit;
}
?>