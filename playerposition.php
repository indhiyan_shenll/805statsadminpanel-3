<?php
$playerposition='';

 if($sportname=='basketball'){ 
	$playerposition ='<option value="G">G</option>
		<option value="C">C</option>
		<option value="F">F</option>
		<option value="SG">SG</option>
		<option value="GF">GF</option>
		<option value="PG">PG</option>
		<option value="PF">PF</option>
		<option value="SF">SF</option>';
} 

if($sportname=='baseball' || $sportname=='softball'){ 
	$playerposition ='<option value="Extra fldr">Extra fldr</option>
		<option value="Pitcher">Pitcher</option>
		<option value="Catcher">Catcher</option>
		<option value="First Base">First Base</option>
		<option value="Second Base">Second Base</option>
		<option value="Third Base">Third Base</option>
		<option value="Shortstop">Shortstop</option>
		<option value="Left Field">Left Field</option>
		<option value="Center Field">Center Field</option>
		<option value="Right Field">Right Field</option>
		<option value="Des Hitter">Des Hitter</option>
		<option value="Des Player">Des Player</option>
		<option value="Pinch Hit">Pinch Hit</option>
		<option value="Pinch Run">Pinch Run</option>
		<option value="Infielder">Infielder</option>
		<option value="Outfielder">Outfielder</option>
		<option value="Utility">Utility</option>';
 } 

 if($sportname=='football'){ 
	$playerposition ='<option value="C">C</option>
		<option value="C/G">C/G</option>
		<option value="CB">CB</option>
		<option value="DB">DB</option>
		<option value="DE">DE</option>
		<option value="DL">DL</option>
		<option value="DT">DT</option>
		<option value="FB">FB</option>
		<option value="FS">FS</option>
		<option value="G">G</option>
		<option value="G/T">G/T</option>
		<option value="K">K</option>
		<option value="KR">KR</option>
		<option value="LB">LB</option>
		<option value="NT">NT</option>
		<option value="OL">OL</option>
		<option value="P">P</option>
		<option value="PR">PR</option>
		<option value="QB">QB</option>
		<option value="RB">RB</option>
		<option value="S">S</option>
		<option value="SS">SS</option>
		<option value="T">T</option>
		<option value="TE">TE</option>
		<option value="WR">WR</option>';
 } 

if($sportname=='soccer'){ 
	$playerposition ='<option value="D">D</option>
		<option value="F">F</option>
		<option value="G">G</option>
		<option value="MF">MF</option>';
} 

 if($sportname=='volleyball'){ 
	$playerposition ='<option value="DS">DS</option>
		<option value="LIBERO">LIBERO</option>
		<option value="LSH">LSH</option>
		<option value="MB">MB</option>
		<option value="MH">MH</option>
		<option value="OH">OH</option>
		<option value="OPPOSITE">OPPOSITE</option>
		<option value="PASSER">PASSER</option>
		<option value="RSH">RSH</option>
		<option value="SETTER">SETTER</option>';
 } 

if(isset($_GET['sport'])){
	$sportname= $_GET['sport'];
}
 $signleplayformentry = '				
			<form action="" class="form-inline multipleplayerformgrp" method="POST" enctype="multipart/form-data">
				<div class="portlet light ">
					<div class="portlet-body form">
						<div class="form-body bulkplayerformbody">
							<div class="">
								<div class="uploadstatus" style="display:inline-block;width:16px;height:16px;">
									<img src="images/loading-publish.gif" style="width:16px;height:16px;" class="uploadstatusimg">
								</div>
								<div class="form-group">
									<label for="team">Team<span class="cs_mandatory">*</span></label>
									<select name="team" class="form-control" id="teaminput">
									<option value="">Select Team</option>'.$TeamLists.'</select>
								</div>
								<div class="form-group">
									<input type="hidden" name="addplayer" value="" >
									<input type="hidden" name="playerid" id="existplayedid" value="" >
									<input type="hidden" name="cid" value="'.$cid.'" >
									<input type="hidden" name="teamid" value="'.$cid.'" >
									<input type="hidden" name="sport" value="'.$sportname.'" >

									<label for="firstname">First Name<span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control ampl_width90 plnew_firstname" maxlength="25" id="firstname" name="firstname" value="" placeholder="Firstname *">
								</div>
								<div class="form-group">
									<label for="lastname">Last Name<span class="cs_mandatory">*</span></label>
									<input type="text" class="form-control ampl_width90 plnew_lastname" maxlength="25" id="lastname" name="lastname" value="" placeholder="Lastname *">
								</div>
								
								<div class="form-group">
									<label for="gender">Gender</label>
									<select class="form-control" id="gender" name="gender">
										<option value="male">Male</option>
										<option value="female">Female</option>
									</select>
								</div>
								<div class="form-group">
									<label for="plheight">Height</label>
									<input type="text" class="form-control ampl_width70" maxlength="25" id="plheight" name="plheight" value="" placeholder="Height">
								</div>
								<div class="form-group">
									<label for="plheight">Weight</label>
									<input type="text" class="form-control ampl_width70" maxlength="25" id="plweight" name="plweight" value="" placeholder="Weight">
								</div>
								<div class="form-group">
									<label for="pl_dob">Date of Birth</label>
									<input type="text" class="form-control pl_dobmask ampl_width95" maxlength="25" id="pl_dob" name="pl_dob" value="" placeholder="Date of Birth">
								</div>
								<div class="form-group">
									<label for="pl_age">Age</label>
									<input type="text" class="form-control ampl_width60" maxlength="25" id="pl_age" name="pl_age" value="" placeholder="Age">
								</div>
								
								<div class="form-group">
									<label for="school">School</label>
									<input type="text" class="form-control ampl_width70 " id="school" name="school" value="" placeholder="School">
								</div>
								<div class="form-group">
									<label for="grade">Grade</label>
									<input type="text" class="form-control ampl_width70" id="grade" maxlength="10" name="grade" value="" placeholder="Grade">
								</div>
								<div class="form-group">
									<label for="prevschool">Previous School<span style="font-weight:normal;font-size: 11px;">(if applicable)</span></label>
									<input type="text" class="form-control ampl_width100" id="prevschool" name="prevschool" value="" placeholder="Previous School">
								</div>						

								<div class="form-group">
									<label for="hometown">Hometown</label>
									<input type="text" class="form-control ampl_width90" maxlength="40" id="hometown" name="hometown" value="" placeholder="Hometown">
								</div>
								<div class="form-group">
									<label for="uniform">Uniform No.*</label>
									<input type="text" class="form-control ampl_width100 uniformint"  maxlength="2" id="uniform" name="uniform" value="" placeholder="Uniform No *">
								</div>
								<div class="form-group">
									<label for="position">Position</label>
									<select class="form-control" id="position" name="position">'.$playerposition.'
									</select>
								</div>
								
								<div class="form-group">
									<label for="playernotes">Player Notes or Bio</label>
									<textarea class="form-control" id="playernotes" rows="10" maxlength="500" name="playernotes" placeholder="Player Notes or Bio"></textarea>
								</div>

								<div class="form-group">
									<span style="font-weight:normal;font-size: 11px;"> Player Image (No larger than 300kb)</span>
									<input type="file" id="file" name="file" class="playerimg" >	
									 <!-- <img class="imgprvw" alt="Preview Image" height="50px" width="50px" border="0"> -->
								</div>				
							</div>
						</div>
					</div>
				</div>
			</form>';
?>






