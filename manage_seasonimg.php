<?php 
session_start();
if($_SESSION['loginid']=='')  {
	header("Location:login.php");
	exit;
}

$cid="";
if($_SESSION['loginid']!='')  {
  if($_SESSION['usertype']=='user' || $_SESSION['signin'] == 'teammanager') {
        $cid=$_SESSION['loginid'];
  }
  else{ 
	  header("Location:login.php");
	   exit;
  }
}
include_once('connect.php');

if(isset($_POST['action'])){
	
	$profileimgid   =  $_REQUEST['profileimgid'];

	if($_POST['action']=='delete'){
		
		$DeleteQry = "delete from player_images where player_img_id=:profileimgid";
		$prepDeleteQry  = $conn->prepare($DeleteQry);
    	$Delete_results = array(":profileimgid"=>$profileimgid);
    	$deleteRes      = $prepDeleteQry->execute($Delete_results);
		
	    if($deleteRes){
			echo "success###Deleted";
		}else{
			echo "failure";
		}
	}else if($_POST['action']=='update'){
		$PlayerId       = $_POST['playerid'];
		$profileimgid   = $_POST['profileimgid'];
		$UpdSeason      = $_POST['selectedvalue'];
		$SelectedOptions= $SelectSeasonName = $SelectedOptions2 = '';
		

		$created_date	= date("Y-m-d H:i:s");
		$TeamQry		= "select * from player_info where id=:PlayerId";
		$get_Team_qry = $conn->prepare($TeamQry);
	    $get_Team_qry->execute(array(":PlayerId"=>$PlayerId));
	    $get_Team_Count = $get_Team_qry->rowCount();
		$TeamRes=$get_Team_qry->fetch();
		$TeamId			= $TeamRes['team_id'];
		$SeasonIds      = "##".implode("##",$UpdSeason)."##";

        $update_results=array(":PlayerId"=>$PlayerId,":cid"=>$cid, ":SeasonIds"=>$SeasonIds, ":created_date"=>$created_date, ":profileimgid"=>$profileimgid);

        $updateQry="update player_images set player_id=:PlayerId, customer_id=:cid,	season_id=:SeasonIds, modified_date=:created_date where player_img_id=:profileimgid";

        $prepupdateQry=$conn->prepare($updateQry);
		$updateRes=$prepupdateQry->execute($update_results);
		
        $PlayerImgIds = "select * from player_images where player_img_id=:profileimgid";
		$getPlayerQry    =   $conn->prepare($PlayerImgIds);
        $getPlayerQry->execute(array(":profileimgid"=>$profileimgid));
        $ProfileCnt      =   $getPlayerQry->rowCount();
		$SeasonIdsArr = array();
		if ($ProfileCnt){
			 $getPlayersRows     =   $getPlayerQry->fetchAll();
             foreach($getPlayersRows as $ImgList){
				$ProfileImgId= $ImgList['player_img_id'];
				$SeasonIdArr  = array_filter(explode("##",$ImgList['season_id']));		
				$SeasonId    = implode(",",$SeasonIdArr);	
				
				$sealist="select * from customer_season where custid=:cid";
				$getseaQry      =   $conn->prepare($sealist);
                $getseaQry->execute(array(":cid"=>$cid));
                $getseaCount      =   $getseaQry->rowCount();
                $getseaRows     =   $getseaQry->fetchAll();
                foreach($getseaRows as $sowlist){
					if(in_array($sowlist["id"],$SeasonIdArr)){
						$Selected  = 'selected';
						$SelectSeasonName .="<p style='margin-top:0px'>".$sowlist["name"]."</p>";
					}else{
						$Selected  = '';
					}			
					$SelectedOptions .='<option value="'.$sowlist["id"].'" '.$Selected.'>'.$sowlist["name"].'</option>';
				 } 	
			}
		}else{
			$sealists="select * from customer_season where custid=:cid";
			$getsealistQry      =   $conn->prepare($sealists);
            $getsealistQry->execute(array(":cid"=>$cid));
            $getsealistCount    =   $getsealistQry->rowCount();
            $getsealistRows     =   $getsealistQry->fetchAll();
                foreach($getsealistRows as $sowlists){							
				$SelectedOptions .='<option value="'.$sowlists["id"].'">'.$sowlists["name"].'</option>';
			} 	
		}
		$PlayerImgIds ="select * from player_images where player_id=:PlayerId";
        $getPlayerImgQry      =   $conn->prepare($PlayerImgIds);
	    $getPlayerImgQry->execute(array(":PlayerId"=>$PlayerId));
	    $getPlayerImgCount      =   $getPlayerImgQry->rowCount();
	    
	    if ($getPlayerImgCount>0){
			$getPlayerImgRows     =   $getPlayerImgQry->fetchAll();
			foreach($getPlayerImgRows as $ImgList ){
				$SeasonIdArr  = array_filter(explode("##",$ImgList['season_id']));
				$SeasonIdsArr  = array_merge($SeasonIdsArr,$SeasonIdArr);					
			}
		}
		//print_r($SeasonIdsArr);

		$sealistQry="select * from customer_season where custid=:cid";
		$getlistQry    =   $conn->prepare($sealistQry);
	    $getlistQry->execute(array(":PlayerId"=>$PlayerId));
        $getlistCount      =   $getlistQry->rowCount();
	    $getsealistRows     =   $getlistQry->fetchAll();
		foreach ($getsealistRows as $sowlistimg ) {			
			$Selected  = in_array($sowlistimg["id"],$SeasonIdsArr)?' disabled':'';								
			$SelectedOptions2 .='<option value="'.$sowlistimg["id"].'" '.$Selected.'>'.$sowlist["name"].'</option>';
		 } 
		$UpdSeasonJson  = json_encode($SeasonIdsArr);
		$SelectedSeason = '<select name="seasons" class="profileimgsedit" multiple="multiple">'.$SelectedOptions.'</select><script>$(document).ready(function(){$(".profileimgsedit").multiselect({includeSelectAllOption: false,numberDisplayed: 1,nonSelectedText: "Select season",});});</script>';
		echo "success###Updated###$SelectSeasonName$SelectedSeason###$SelectedOptions2";
		//echo "$SelectedOptions2";
	}
}
exit;
 ?>
