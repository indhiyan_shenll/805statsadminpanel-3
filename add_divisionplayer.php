<?php 
include_once('session_check.php');
include_once('connect.php'); 
include_once('header.php'); 

$Sports = array();
$SportListArr = array();
$SportsLists = $conn->prepare("select * from customer_subscribed_sports where customer_id=:customer_id");
$SportListArr = array(":customer_id"=>$Cid);
$SportsLists->execute($SportListArr);
$CntSportsLists = $SportsLists->rowCount();
if ($CntSportsLists > 0) {
    $SporstRes = $SportsLists->fetchAll(PDO::FETCH_ASSOC);
    foreach ($SporstRes as $SporstRow) {
        $Sports[]= $SporstRow['sport_id']; 
    }

    if ($Sports[0]=='4444') { $tablename='team_stats_bb'; } 
    if ($Sports[0]=='4442' || $Sports[0]=='4441') { $tablename='team_stats_ba'; } 
    if ($Sports[0]=='4443') { $tablename='team_stats_fb'; }
}

?>
<link href="assets/custom/css/addplayertoseason.css" rel="stylesheet" type="text/css" />
    <!-- BEGIN CONTENT -->
	
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <div class="row">                
                    <div class="col-md-12">
                        <div class=" left-right-padding">
                            <div class="row searchheder">                
                                <div class="col-md-12 searchbarstyle">
								<form id="addplayerform" method="POST">
									<div class="col-md-2 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group caption font-red-sunglo selecttext">
											<span class="caption-subject bold uppercase">Select season</span>
										</div>									
									</div>

									<div class="col-md-4 col-sm-3 col-xs-12">
										<div class="form-group ">											
											<select class="form-control  border-radius" name="seasonlist" id="seasonlist">
											<option value=''>Select season</option>
											<?php
											$Qry		= $conn->prepare("select * from customer_season where custid=:custid order by season_order desc");
											$Qryarr		= array(":custid"=>$customerid);
											$Qry->execute($Qryarr);
											$QryCntSeason = $Qry->rowCount();
											$DivisionWrapHtml= $AddNewSeasonTree='';
											$Inc =0;
											if ($QryCntSeason > 0) {
												while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){									
													echo "<option value='".$row['id']."'>".$row['name']."</option>";
												}
											}else{
												echo "<option value=''>No season found</option>";
											}
											?>											
											</select>
											
											<script>$("#seasonlist").val("<?php echo $_SESSION['seasonid'];?>");</script>
										</div>
									</div>

									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="conferencelist" id="conferencelist">
												<option value=''>Select conference</option>	
												<?php
													$Qry		= $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id");
													$Qryarr		= array(":season_id"=>$_SESSION['seasonid']);
													$Qry->execute($Qryarr);
													$QryCntSeason = $Qry->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['conference_name']."</option>";
														}
													}else{
														echo "<option value=''>No conference found</option>";
													}
												?>
											</select>
											
											<script>$("#conferencelist").val("<?php echo $_SESSION['conferenceid'];?>");</script>
										</div>									
									</div>
									<div class="col-md-3 col-sm-3 col-xs-12 removerightpadding">							
										<div class="form-group">
											<select class="form-control  border-radius requiredcs" name="divisionlist" id="divisionlist">
												<option value=''>Select division</option>	
												<?php
													$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id");
													$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid']);

													$QryExeDiv->execute($QryarrCon);
													$QryCntSeason = $QryExeDiv->rowCount();
													$DivisionWrapHtml= $AddNewSeasonTree='';
													$Inc =0;
													if ($QryCntSeason > 0) {
														while ($row = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){							
															echo "<option value='".$row['id']."'>".$row['name']."</option>";
														}
													}else{
														echo "<option value=''>No season found</option>";
													}
												?>
											</select>
											<script>$("#divisionlist").val("<?php echo $_SESSION['divisionid'];?>");</script>
										</div>									
									</div>									
									</form>
								</div>
							</div>
                        </div>
                        
                        <!-- BEGIN SAMPLE FORM PORTLET-->
						<div class=" addteammainwrap">                               
							<div class="portlet-body form">
								<div class="form-body top-padding" style="padding: 0px 15px 0px 15px"> 
									<!-- <h4 id="demo-undo-redo">Undo / Redo</h4> -->
									<div class="row">

										<div class="col-xs-12 col-sm-5 col-md-5 padd-division" style="padding-left: 0px;">
											<div class="portlet light">
											<select name="from[]" id="undo_redo" class="form-control border-radius " size="13">
											<?php	
												$QryExeTeam = $conn->prepare("select * from customer_division_team as divteam LEFT JOIN teams_info as custteam ON  divteam.team_id=custteam.id where divteam.conference_id=:conference_id and divteam.season_id=:season_id and divteam.division_id=:division_id");
												$QryarrCon = array(":conference_id"=>$_SESSION['conferenceid'],":season_id"=>$_SESSION['seasonid'],":division_id"=>$_SESSION['divisionid']);

												$QryExeTeam->execute($QryarrCon);
												$QryCntSeason = $QryExeTeam->rowCount();									
												
												if ($QryCntSeason > 0) {
													while ($rowTeam = $QryExeTeam->fetch(PDO::FETCH_ASSOC)){
														echo "<option value='".$rowTeam['team_id']."' >".$rowTeam['team_name']."</option>";
													}
												}
											?>
											</select>
											
											</div>

										</div>
										
										
										
										<div class="col-xs-12 col-sm-7 col-md-7 rightsidewrap paddleft-division"  style="padding-right: 0px;">
											<div class="portlet light">
											<div id="undo_redo_to" class="form-control border-radius requiredcs" size="13" >

											</div>
											
											</div>
										</div>
									</div>
								</div> 
								
								 
							</div>
						</div>           
                    </div>                    
                
            </div>            
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT --> 
</div>

<!-- END CONTAINER -->

<!-- Division model popup -->
<div id="PlayerModal" class="modal fade large player-model" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add Player</h4>
	  </div>
	  <div class="modal-body">
		
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>


<!-- Division model popup -->
<div id="SwitchTeamModal" class="modal fade large" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Switch Team</h4>
	  </div>
	  <div class="modal-body">
		<form id="switchteamform" method="post">
			<input type="hidden" name="playeridhid" id="playeridhid">
			<input type="hidden" name="playerexsisthid" id="playerexsistidhid" value="">

		  <div class="col-md-10" style="margin:auto;float:none;">				
				<div class="form-group col-md-12 ">
					<label>Select Team<span class="error">*</span></label>
					<select name="switchteamid" class="form-control" id="switchteamid">		
					<?php 
					if ($_SESSION['master'] == 1) { 
						$children = array($_SESSION['childrens']);					
						//$ids = join(',',$children);
						 $ids = $_SESSION['loginid'].",".join(',',$children);
						 $res = "SELECT * FROM teams_info WHERE (customer_id IN ($ids) or customer_id IN ($ids))  and (sport_id='$SportId') order by team_name"; 
					} else {
						$res = "select teams_info.* from teams_info  LEFT JOIN $tablename ON teams_info.id=$tablename.teamcode where teams_info.customer_id in ($customerid) and team_name!='' group by teams_info.id order by teams_info.team_name";
					}
					
					$getResQry      =   $conn->prepare($res);
					$getResQry->execute();
					$getResCnt      =   $getResQry->rowCount();
					
					if ($getResCnt > 0) {
						$getResRows     =   $getResQry->fetchAll(PDO::FETCH_ASSOC);
						echo "<option value=''>Select Team</option>";
						foreach($getResRows as $team){
							echo "<option value='".$team['id']."'>".$team['team_name']."</option>";
						}
					}else{
					
					}
					?>
					</select>
					<label id="switchteamid-error" class="error" for="switchteamid">Please select team</label>
				</div>	
					
				<div class="form-group col-md-12 popupbtn">										
					<input class="btn switchteambtn btn-success" type="button" value="Save/Update">
					<button class="btn cancelbtn btn-danger" type="button" data-dismiss="modal">Cancel</button>
				</div>	
			</div>
		</form>
		 <table width='100%' id="loadingswitchtam"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>
		 <table width='100%' id="teamswitchmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players switched to other team successfully..</td></tr></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<div class="modal fade" id="SwitchUpdateModel" tabindex="-1" role="basic" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close btn_close" data-dismiss="modal" aria-hidden="true"></button>
                <h4 class="modal-title">Update Player Info</h4>
            </div>
            <div class="modal-body">
            	<div class="wrongstatus">
            	</div>
            	<form name="frm_switchteam" id="frm_switchteam" method="POST" enctype="multipart/form-data">
            		<input type="hidden" id="hnd_player_id" name="hnd_player_id"> 
            		<input type="hidden" id="hnd_teamid" name="hnd_teamid">
            		<input type="hidden" id="hnd_seasionid" name="hnd_seasionid">
            		<input type="hidden" id="hnd_divisionid" name="hnd_divisionid">
            		<input type="hidden" id="hnd_conferenceid" name="hnd_conferenceid">
            		<input type="hidden" id="hnd_image" name="hnd_image">
            		<div class="row switchupdateplayer" >
            			<div class="form-group col-md-10 form_swith">
	            			<div class="form-group col-md-6 form_uniform">
	            				<label>Uniform No</label>
	            				<input type="text" name="uniform_no" id="uniform_no" class="form-control border-radius" value="">
	            				<label id="uniformnoid-error" class="error" for="uniformnoid" style="margin-bottom:0px;"> Please enter uniform number</label>
	            				<label id="uniformid-error" class="error" for="uniformid" style="margin-bottom:0px;">Please enter number only</label>
	            			</div>
	            			<div class="col-md-6 form_position"> 
	                         	<div class="form-group">
	                             <label>Position</label>
	                             <select class='form-control border-radius' id='position' name='position'>
	                             </select>
	                             <label id="positionid-error" class="error" for="positionid" style="margin-bottom:0px;">Please select position</label>

	                           </div>
	                        </div> 
                        </div>  
            			<div class="form-group col-md-10 form_swith" style="overflow:auto">
							<label>Select Team</label>
							<select name="sel_switch" id="sel_switch" class="form-control border-radius" >
			            	</select>
						</div>
						<div class="form-group col-md-10 form_swith" style="margin-top: 15px;">
							<label>Player Image</label>
							<input type="file" name="upload_file" id="upload_file">
						</div>

						<div class="form-group col-md-10 form_swith" style="margin-top: 15px;">
                            <label>Status</label>
                            <div class="mt-radio-inline">
                                <label class="mt-radio status_radio"> Active
                                    <input type="radio" value="1" name="isactive" id="isactive-yes" checked >
                                    <span></span>
                                </label>
                                <label class="mt-radio status_radio"> In-Active
                                    <input type="radio" value="0" name="isactive" id="isactive-no">
                                    <span></span>
                                </label>
                            </div>
                        </div>
						<div class="form-group col-md-10 form_swith" style="margin-top: 10px;">			    
							<input class="btn btn-success switch_btn" type="button" value="Submit">
							<button class="btn btn-danger" type="button" data-dismiss="modal" style="margin-left: 15px;">Cancel</button>
						</div>
            		</div>
				</form>
				 <table width='100%' id="loadingplayers"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Assign players to team... Please wait...</td></tr></table>
				 <table width='100%' id="playerswichmsg"><tr><td align='center' style='font-size:15px;color:green;'>Players details updated successfully..</td></tr></table>
            </div>
			<!-- <div style="height:20px;"></div> -->
            <div class="modal-footer">
                <button type="button" class="btn btn-danger btn_close" data-dismiss="modal">Close</button>
            </div>
        </div>
        <!-- /.modal-content -->
    </div>
    <!-- /.modal-dialog -->
</div>
<!-- /.modal -->


<style>
#loadingplayer,#playerstsmsg,#switchteamid-error,#loadingplayers,#playerswichmsg,
#uniformnoid-error,#positionid-error, #uniformid-error{
	display:none;
}
.modal-body{
	overflow:auto;
}
#switchteamid{
	border-radius:4px !important;
}
.form_position{
	padding-right:0px;
	
} 
.form_uniform{
	padding-left:0px;
}
.switchteambtn {
	color: #ffffff;
	background-color: #549E39;
	background-image: -moz-linear-gradient(top, #A1C873, #70A62F);
	background-image: -ms-linear-gradient(top, #A1C873, #70A62F);
	background-image: -webkit-gradient(linear, 0 0, 0 100%, from(#A1C873), to(#70A62F));
	background-image: -webkit-linear-gradient(top, #A1C873, #70A62F);
	background-image: -o-linear-gradient(top, #A1C873, #70A62F);
	background-image: linear-gradient(top, #A1C873, #70A62F);
	filter: progid:DXImageTransform.Microsoft.gradient(startColorstr='#A1C873', endColorstr='#70A62F', GradientType=0);
	border-color: #619126;
	border-radius: 4px !important;
	border-color: rgba(0, 0, 0, 0.25) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.35) rgba(0, 0, 0, 0.25);
}
#switchteamid-error,#uniformnoid-error,#positionid-error,#positionid-error,#uniformid-error{
	color:red;
}
.playerimgtbl img{
	    border-radius: 50px !Important;
}
.playerimgtbl{
    padding: 5px !important;
}
.assignplayertbl tr:nth-child(even) {
	    background-color: rgba(158, 158, 158, 0.1);
		height:50px;
}
.assignplayertbl tr:nth-child(odd) {
	        background-color: rgba(158, 158, 158, 0.04);
			height:50px;
}
.status_radio{
    margin-bottom: 0px !important;
}
.btn_managesea{
    margin-bottom: 6px !important;
}
.alert-danger {
    background-color: #fbe1e3;
    border-color: #fbe1e3;
    color: #e73d4a;
    padding: 10px;
    border-radius: 5px !important;
}

.form_swith{
	margin:auto;
	float:none;
}
label {
    font-weight: 400;
    font-weight: 600;
    margin-bottom: 10px;
}
.backbtnred,.backbtnred:hover{
	background-image: -webkit-linear-gradient(top, #E95D5D, #E40304);
	color: #FFF;
	padding: 5px 13px;
	border-radius: 3px !important;
	cursor: pointer;
	font-size: 12px;
}
</style>
<?php include_once('footer.php'); ?>

<script type="text/javascript" src="assets/global/plugins/multiselect.js"></script>
<script type="text/javascript" src="assets/custom/js/adddivisionplayer.js"></script>
<!-- END PAGE LEVEL PLUGINS -->

<!-- END PAGE LEVEL SCRIPTS -->
