<?php 
include_once('session_check.php');
include_once('connect.php'); 
include_once('header.php'); 
?>
<link href="assets/custom/css/manageseason.css" rel="stylesheet" type="text/css" />
<script src="https://code.jquery.com/ui/1.12.1/jquery-ui.js"></script>
    <!-- BEGIN CONTENT -->
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            
            <div class="row">            	
				<div class="col-md-12 ">
					<!-- BEGIN SAMPLE FORM PORTLET-->
					<div class="portlet light headertopbox" style="margin-bottom: 10px;padding-bottom: 10px;">
						<div class="portlet-title">
							<div class="caption font-red-sunglo">
								<i class="icon-settings font-red-sunglo"></i>
								<span class="caption-subject bold uppercase"> Season / Conference / Division</span>
							</div>
							<button type="button" class="btn uppercase addseasonbtntop" data-toggle="modal" data-target="#seasonModal"  style='float: right;'>Add season</button>
						</div>
					</div>

					 <div class="portlet light bottommainwrap" style="overflow: auto;padding: 12px 0px;">
						<div class="portlet-body form seasonmainwrapper" style="overflow:hidden;">							
						<?php			
							
							$Qry		= $conn->prepare("select * from customer_season where custid=:custid order by season_order ASC");
							$Qryarr		= array(":custid"=>$customerid);
							$Qry->execute($Qryarr);
							$QryCntSeason = $Qry->rowCount();
							$DivisionWrapHtml= $AddNewSeasonTree='';
							$CopyFromSeason = false;
							$Inc =0;
							if ($QryCntSeason > 0) {
								$CopyFromSeason = true;
								while ($row = $Qry->fetch(PDO::FETCH_ASSOC)){

									$QryExe = $conn->prepare("select * from customer_season_conference as seasonconf LEFT JOIN customer_conference as custconf ON  seasonconf.conference_id=custconf.id where season_id=:season_id order by  seasonconf.conf_order ASC");
									$Qryarr = array(":season_id"=>$row['id']);
									$QryExe->execute($Qryarr);
									$QryCntSeasonconf	= $QryExe->rowCount();											
									$SeletedArrConf		= array();
									$SeletedArrDiv		= array();
									$AddNewSeason = $AddSeasontbl = $Conferencetbl= '';
									
									if ($QryCntSeasonconf > 0) {
										while ($rowSeason = $QryExe->fetch(PDO::FETCH_ASSOC)){												
											$QryExeDiv = $conn->prepare("select * from customer_conference_division as seasonconfdiv LEFT JOIN customer_division as custconf ON  seasonconfdiv.division_id=custconf.id where seasonconfdiv.conference_id=:conference_id and season_id=:season_id order by seasonconfdiv.div_order ASC");
											$QryarrCon = array(":conference_id"=>$rowSeason['conference_id'],":season_id"=>$row['id']);
											$QryExeDiv->execute($QryarrCon);
											$QryCntSeasonconf = $QryExeDiv->rowCount();
											$Divisiontbl = $AddNewSeason='';
											while ($rowSeasonDiv = $QryExeDiv->fetch(PDO::FETCH_ASSOC)){													
												$Selected = ($rowSeasonDiv['status'])?'checked':'';
												$Divisiontbl .= "<div class='divisions' data-divid='".$rowSeasonDiv['id']."'><table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='division[]'  value='".$rowSeasonDiv['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtndiv tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete division' data-divisionid='".$rowSeasonDiv['id']."' data-conferenceid='".$rowSeason['id']."' data-seasionid='".$row['id']."' ><i class='icon-trash'></i></a><a href='add_divisionteam.php?divisionid=".$rowSeasonDiv['id']."&conferenceid=".$rowSeason['id']."&seasonid=".$row['id']."' class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' data-container='body' data-placement='top' data-original-title='Add Team'><i class='fa fa-plus'></i></a></td></tr></table></div>";	
												$SeletedArrDiv[] = $rowSeasonDiv['id'];
												
												$AddNewSeason .= "<table class='table innerdivtable'><tr><td class='divisionbtns'><span class='divisioncircle circle'>D</span><label class='mt-checkbox'><input type='checkbox' name='season[conference".$rowSeason['id']."][division".$rowSeasonDiv['id']."]' value='".$rowSeasonDiv['id']."###".$rowSeason['id']."' $Selected> ".$rowSeasonDiv['name']."<span></span></label></td></tr></table>";	
											}

											$Selected = ($rowSeason['status'])?'checked':'';
											$Conferencetbl .= "<div class='conferencetbltoggle' data-conf='".$rowSeason['id']."'><table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox '><input type='checkbox' name='' value='".$rowSeason['id']."' $Selected> ".$rowSeason['conference_name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deletebtnconf tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete conference' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-trash'></i></a><a class='btn btn-circle btn-icon-only btn-default blue managedivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Manage division'  data-target='#managedivModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green adddivisionbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Add division' data-toggle='modal' data-target='#DivisionModal' data-conferenceid='".$rowSeason['id']."' data-seasonid='".$row['id']."'><i class='fa fa-plus'></i></a></td></tr></table><div class='divisiontbltoggle'>".$Divisiontbl."</div></div>";	

											$AddSeasontbl .= "<table class='table innertable' id='innertblid".$rowSeason['id']."'><tr><td class='conferencebtns '><span class='conferencecircle circle'>C</span><label class='mt-checkbox'><input type='checkbox' name='season[conference".$rowSeason['id']."]' value='".$rowSeason['id']."###".$row['id']."' $Selected class='conferencechkbox'> ".$rowSeason['conference_name']."<span></span></label></td></tr></table>".$AddNewSeason;	
											$SeletedArrConf[] = $rowSeason['id'];
											$SeletedArrDiv		= array();
										}
									}									

									

									$SeasonTree = "<table class='table  ms_seasontble' data-seasion='".$row['id']."'><tr><td><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='' value='".$row['id']."'> ".$row['name']."<span></span></label><a class='btn btn-circle btn-icon-only btn-default red deleteseasonbtn tooltips' href='javascript:;' data-container='body' data-placement='top' data-original-title='Delete season' data-seasonid='".$row['id']."'><i class='icon-trash' ></i></a><a class='btn btn-circle btn-icon-only btn-default blue    manageconferencemodel tooltips' href='javascript:;'  data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Manage conference'><i class='icon-wrench'></i></a><a class='btn btn-circle btn-icon-only btn-default green addconferencebtn tooltips' href='javascript:;' data-toggle='modal' data-target='#ConferenceModal' data-seasonid='".$row['id']."' data-container='body' data-placement='top' data-original-title='Add conference'><i class='fa fa-plus'></i></a><div class='parent_confernce'>$Conferencetbl</div></td></tr></table>";


									$AddNewSeasonTree .= "<div class='mt-radio-list clearfix'><label class='mt-radio'><input type='radio' name='seasonnamesnewid' value='".$row['id']."' class='seasonnamesnew'>".$row['name']."<span></span></label></div><table class='table  ms_seasontble popupmodelseason'><tr><td><form method='POST' class='selectedseasontree' novalidate='novalidate'><span class='seasoncircle circle'>S</span><label class='mt-checkbox'><input type='checkbox' name='seasonnames[]' value='".$row['id']."' checked disabled> ".$row['name']."<span></span></label>$AddSeasontbl</form></td></tr></table>";						

									echo '<div class="col-md-12 col-sm-12 seasonwrapcont" id="seasonmaincont_'.$row['id'].'" data-seasonid="'.$row['id'].'"><div class="portlet box grey seasontbltogglewrap">
										<div class="portlet-title">
											<div class="caption tools" style="width: 98%;">
												<a href="javascript:;" class="expand" style="color:#000;background-image:none;display: block;width: 100%;"> '.$row['name'].'</a>
											</div>												
											<div class="tools">
												<a href="javascript:;" class="expand" style=""></a>
											</div>
										</div>
										<div class="portlet-body seasontbltoggle" style="display:none;">'.$SeasonTree.$DivisionWrapHtml.'
										</div>
									</div></div>';
									$conference='';
									$Inc++;
								} 	
							}else{
								echo '<div class="col-md-12 col-sm-12 seasonwrapcont donotdraganddrap text-center">
									<div class="portlet box grey seasontbltogglewrap">
										<div class="portlet-title">
											<div class="caption tools" style="width: 98%;">
												<a href="javascript:;" class="collapse" style="color:#000;background-image:none;display: block;width: 100%;">No season(s) found</a>
											</div>
										</div>
									</div>
								</div>';
							}							   
							?>							
						  </div> 
						</div>
					</div>               
            </div>
        </div>
        <!-- END CONTENT BODY -->
    </div>
    <!-- END CONTENT -->                
</div>
<!-- END CONTAINER -->

<!-- Manage conference popup model -->
<div id="ManageConferenceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Manage Conference</h4>
	  </div>
	  <div class="modal-body">
	  <form name="manageconfform" id="manageconfformid" method="POST" class="form-horizontal" role="form">
		  <div class="row">
			  <div class="col-md-11 btn_managedivision" style="margin:auto;float:none; padding-bottom: 6px;">									  
				  <input type="hidden" name="seasionidconf" value="" id="seasionidconfid">
				  <div id="manageconfformcont" style="padding-bottom: 6px;">
				  </div>				 
				  <input class="btn btn-success manageconfpopbtn" type="button" value="Submit">
				  <button class="btn btn-danger btn_cancel cancelbtn" type="button" data-dismiss="modal">Cancel</button>					  
			  </div>
		  </div>
	  </form>
	  <table width='100%' id="loadingconferenceselected"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading conference... Please wait...</td></tr></table>
	   <table width='100%' id="loadingconference"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving conference... Please wait...</td></tr></table>

		<table width="100%" id="manageconfmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Conference details updated successfully.</td></tr></tbody></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<!-- Manage division popup model -->
<div id="managedivModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Manage Division</h4>
	  </div>
	  <div class="modal-body">
		  <form name="managecdivform" id="managedivformid" method="POST" class="form-horizontal" role="form">
			  <div class="row">
				  <div class="col-md-11 " style="margin:auto;float:none;padding-bottom: 6px;">									  
					  <input type="hidden" name="seasioniddiv" value="" id="seasioniddivid">
					  <input type="hidden" name="seasionidconfdiv" value="" id="seasionidconfdivid">
					  <div id="managedivformcont" style="padding-bottom: 6px;">
					  </div>				 
					  <input class="btn btn-success managedivpopbtn" type="button" value="Submit">
					  <button class="btn btn-danger btn_cancel cancelbtn" type="button" data-dismiss="modal">Cancel</button>					  
				  </div>
			  </div>
		  </form>
		 <table width='100%' id="loadingdivisionselected"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading division... Please wait...</td></tr></table>	
		  <table width='100%' id="loadingdivison"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving division... Please wait...</td></tr></table>
		  <table width="100%" id="managedivinmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Division details updated successfully.</td></tr></tbody></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<!-- Season model popup -->
<div id="seasonModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add new season</h4>
	  </div>
	  <div class="modal-body">
			<div class="col-md-11" style='margin:auto;float:none;'>
				   <table width='100%' id="loadingseasonaddform"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Loading season... Please wait...</td></tr></table>
					<div class="row">
						<form name="addseason" id="addseasonfrm" method="POST" class="form-horizontal" novalidate="novalidate">
							<div class="form-group col-md-12 ">
								<label>Season Name <span class="required" aria-required="true"> * </span></label>
								<input class="form-control requiredcs" type="text" name="seasonnamenew" id="seasonnamenew" placeholder="Season Name" />	
							</div>
						</form>
					</div>
				<div id="addseasonformcont" >
				</div>
				<div class="row" style="margin-top: 25px;" id="addseasonbtnshow">
						<input class="btn addnewseasonbtn" type="button" value="Submit">
						<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal" style="margin-left:15px;">Cancel</button>
				</div>
				<table width='100%' id="loadingseason"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving season... Please wait...</td></tr></table>
				<table width="100%" id="seasonstsnmsg"><tbody><tr><td align="center" style="font-size:15px;color:green;">Season details updated successfully.</td></tr></tbody></table>
			</div>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>

<!-- Conference model popup -->
<div id="ConferenceModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Conference</h4>
	  </div>
	  <div class="modal-body">
	  <form name="addconference" id="conferencefrm" method="POST" class="form-horizontal" novalidate="novalidate">
		  <input type="hidden" name="addconfrencenew" value="addconfrencenew"/> 
		  <input type="hidden" name="seasonid" id="seasonid" /> 
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Conference Name <span class="required" aria-required="true"> * </span></label>
				<input class="form-control requiredcs border-radius" type="text" name="conference" id="conferencename" placeholder="Conference Name" /> 	
				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Status</label>
				<div class="form-group col-md-12 btn_managesea">
					<label class="mt-radio status_radio">
						<input type="radio" name="activeconference" value="1"> Active
						<span></span>
					</label>
					<label class="mt-radio status_radio">
						<input type="radio" name="activeconference" value="0" checked="checked"> Inactive
						<span></span>
					</label>
				</div>
			</div>	
			<div class="form-group col-md-12 btn_managesea">										
				<input class="btn btn-success addnewconferencebtn" type="button" value="Submit">
				<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width='100%' id="loadingaddconfer"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving conference... Please wait...</td></tr></table>

		<table width="100%" id="conferencestsmsg" style="display: none;"><tbody><tr><td align="center" style="font-size:15px;color:green;">Conference details saved successfully.</td></tr></tbody></table>

	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>						

<!-- Division model popup -->
<div id="DivisionModal" class="modal fade" role="dialog">
  <div class="modal-dialog">							
	<div class="modal-content">
	  <div class="modal-header">
		<button type="button" class="close" data-dismiss="modal">&times;</button>
		<h4 class="modal-title">Add New Division</h4>
	  </div>
	  <div class="modal-body">
	  <form name="adddivision" id="divisionfrm" method="POST" class="form-horizontal" novalidate="novalidate">
		  <input type="hidden" name="adddivisionnew" value="adddivision11"/> 
		  <input type="hidden" name="conferenceid" id="conferenceid" /> 
		  <input type="hidden" name="seasonid" id="seasoniddiv" /> 
		<div class="col-md-10" style='margin:auto;float:none;'>
			<div class="form-group col-md-12 ">
				<label>Division Name <span class="required" aria-required="true"> * </span></label>
				<input class="form-control requiredcs border-radius" type="text" name="division" id="divisionname" placeholder="Division Name" /> 				
			</div>	
			<div class="form-group col-md-12 ">
				<label>Division Rule <span class="required" aria-required="true"> * </span></label>
				<select name="divisionrule" class="form-control seldivinrule uniquedivision border-radius " id="divisionrule">
					<option value=''>Select division rule</option>
					<?php								
					$sql		= "select * from customer_division_rule";			
					$stmt = $conn->query($sql); 
					$QryCntRule = $stmt->rowCount();
					if ($QryCntRule > 0) {
						while ($QryCntRuleRow = $stmt->fetch(PDO::FETCH_ASSOC)){
							echo "<option value='".$QryCntRuleRow['id']."'>".$QryCntRuleRow['name']."</option>";	
						}
					}
					?>
				</select>
			</div>	
			<div class="form-group col-md-12 ">
				<label>Status</label>
				<div class="form-group col-md-12 btn_managesea">
					<label class="mt-radio status_radio">
						<input type="radio" name="activedivision" class= "activedivision" value="1"> Active
						<span></span>
					</label>
					<label class="mt-radio status_radio">
						<input type="radio" name="activedivision" class= "inactivedivision" value="0" checked="checked"> Inactive
						<span></span>
					</label>
				</div>
			</div>	
			<div class="form-group col-md-12 btn_managesea">										
				<input class="btn btn-success addnewdivibtn" type="button" value="Submit">
				<button class="btn btn-danger cancelbtn" type="button" data-dismiss="modal">Cancel</button>
			</div>	
		</div>
		</form>
		<table width='100%' id="loadingadds"><tr><td align='center'><img src='assets/custom/imgs/loading.gif' style='margin-right: 10px;width: 75px;'></td><tr><td align='center' style='font-size:15px;color:green;'>Saving division... Please wait...</td></tr></table>

		<table width='100%' id="divisionstsmsg"><tr><td align='center' style='font-size:15px;color:green;'>Division details saved successfully.</td></tr></table>
	  </div>
	  <div class="modal-footer">
		<button type="button" class="btn btn-danger" data-dismiss="modal">Close</button>
	  </div>
	</div>
  </div>
</div>
<?php include_once('footer.php'); ?>
<style>


/*** **/
</style>
<script>


$(document).on("change",".conferencechkbox", function() {
	var ischecked= $(this).is(':checked');
	if(!ischecked){
	    $(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').removeAttr('checked');
	}else{
		$(this).closest(".innertable").nextUntil("table.innertable").find('input[type="checkbox"]').prop('checked' , true);
	}
}); 

</script>
<script src="assets/custom/js/manageseasoncustom.js" type="text/javascript"></script>
