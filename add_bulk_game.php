<?php
include_once("connect.php");
include_once('session_check.php');
include_once('usertype_check.php');  
include_once('header.php');
if (isset($_GET['sport'])) {
    $SportName = $_REQUEST['sport'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
           $SportId = $QrySportVal['sportcode'];
        }
    }    
}


include('gameposition.php');
?>
<link href="assets/custom/css/addbulkentrygame.css" rel="stylesheet" type="text/css">
<link href="assets/global/plugins/bootstrap-daterangepicker/daterangepicker.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-datepicker/css/bootstrap-datepicker3.min.css" rel="stylesheet" type="text/css" />
<link href="assets/global/plugins/bootstrap-timepicker/css/bootstrap-timepicker.min.css" rel="stylesheet" type="text/css" />
<!-- BEGIN CONTENT -->
    <input type="hidden" id="sportid" value="<?php echo $SportId; ?>" name="sportid">
    <input type="hidden" id="sportname" value="<?php echo $SportName; ?>" name="sportname">
    <div class="page-content-wrapper">
        <!-- BEGIN CONTENT BODY -->
        <div class="page-content">
            <!-- BEGIN PAGE HEADER--> 
			<div id="addplayermaincont">

				<div id="addmoreplayercont">
				<?php
					echo $signlegameformentry;
				?>
				</div>
				<div class="col-md-12 left-right-padding">
                    <div class="portlet light info-caption">
                        <div class="portlet-title">
                            <div class="caption font-red-sunglo">
                                <i class="icon-settings font-red-sunglo"></i>
                                <span class="caption-subject bold uppercase"> Game Information</span>
                            </div>
                        </div>
                    </div>
                </div>

	            <div class="col-md-12 bulkplayerparent">
					<div id="bultplayentrycont" class="col-md-12 col-sm-12 col-xs-12 bulkplayerupload">	

						<?php
							for($i=1;$i<=1;$i++){			
								echo $signlegameformentry;	
							}
						?>
						
					</div>
					<div class="addmorebtncont">
						<div class="pull-right">
							<button type="button" class="btn btn-danger deleteplayerbtn customredbtn">Remove</button>
							<button type="button" class="btn btn-success addmoreplayerbtn customgreenbtn">Add more</button>
						</div>
					</div>
					<div class="submitbtncont">
						<div class="pull-left">			
							<button type="button" class="btn btn-danger customredbtn" onclick="window.location='game_list.php'">Back</button>
							<button type="button" class="btn btn-success submitallplayerbtn customgreenbtn">Submit</button>
						</div>
					</div>
				</div>

				<div id="myModal" class="modal fade updateplayerform" role="dialog" data-backdrop="static" data-keyboard="false">
				  <div class="modal-dialog">

					<!-- Modal content-->
					<div class="modal-content">
					  <div class="modal-header">
						<button type="button" class="close" data-dismiss="modal">&times;</button>
						<h4 class="modal-title">Update Player Information</h4>
					  </div>
					  <div class="modal-body" style="text-align:center;">
						<div class="loadingimgcont">
							<img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg">
							<div class="alert alert-success" id="updatemsg" style="display: block;">Details updated successfully</div>
						</div>
					  </div>
					  <div class="modal-footer">
						<button type="button" class="btn btn-default" data-dismiss="modal">Close</button>
					  </div>
					</div>

				  </div>
				</div>
			</div>
			<!-- end #content -->
		</div>
	</div>
</div>
<div class="modal fade " id="SeasonModal" role="dialog">
    <div class="modal-dialog">                                                    
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closeseasonmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title" >Add new season</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">    
                    <label for="addssntext">Enter New Season</label>
                    <input type="text" id="addssntext" class="form-control" name="addssntext" />
                    <span id="seasonerror" class="help-block help-block-error">Please provide a season</span>
                </div>
                <input type="button" name="addssnbtn" class="btn btn-success" value="Submit" id="addssnbtn">
                <button class="btn btn-danger customredbtn cancelbtn closeseasonmodal" type="button" data-dismiss="modal">Cancel</button>
            </div>
        </div>
    </div>
</div>  
<!-- Division Modal -->
<div class="modal fade " id="DivisionModal" role="dialog">
    <div class="modal-dialog">                                                    
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close closedivmodal" data-dismiss="modal">&times;</button>
                <h4 class="modal-title">Add new division</h4>
            </div>
            <div class="modal-body">
                <div class="form-group">    
                    <label for="adddivtext">Enter New Division</label>
                    <input type="text" id="adddivtext" class="form-control" name="adddivtext" />
                    <span id="diverror" class="help-block help-block-error">Please provide a division</span>
                </div>
                <input type="button" name="adddivbtn" class="btn btn-success" value="Submit" id="adddivbtn">
                <button class="btn btn-danger customredbtn cancelbtn closedivmodal" type="button" data-dismiss="modal">Cancel</button>                                    
                
            </div>
        </div>
    </div>
</div>  
<!--- Exist game modal-->
<div id="ExistGame" class="modal fade" role="dialog">
	<div class="modal-dialog">
		<div class="modal-content">
		</div>
  	</div>  
</div> 
<?php include('footer.php');   ?>
<script src="assets/global/plugins/jquery-validation/js/jquery.validate.min.js" type="text/javascript" ></script>
<script src="assets/global/plugins/bootstrap-datepicker/js/bootstrap-datepicker.min.js" type="text/javascript"></script>
<script src="assets/global/plugins/bootstrap-timepicker/js/bootstrap-timepicker.min.js" type="text/javascript"></script>
<script src="assets/pages/scripts/components-date-time-pickers.min.js" type="text/javascript"></script>
<script src="assets/custom/js/bulkgameupload.js" type="text/javascript"></script>
