<?php
include_once("connect.php");
include_once('session_check.php');
include_once('usertype_check.php');
include_once('common_functions.php'); 

if (isset($_POST['gamename']) && (!empty($_POST['gamename']))) {

    $GameName = strip_tags($_POST['gamename']);
    $GameDate = strip_tags($_POST['gamedate']);
    $GameTime = strip_tags($_POST['gametime']);
    $Zone = $_POST['timezone'];
    $Visitor = strip_tags($_POST['visitor']);
    $Home = strip_tags($_POST['home']);
    $Gender = $_POST['gender'];
    $Location = strip_tags($_POST['location']);
    $DivisionList = $_POST['divisionlist'];
    $GameType = $_POST['gametype'];
    $EntryMode = $_POST['entrylist'];
    $SeasonList = $_POST['seasonlist'];
    $Tournament= strip_tags($_POST['tournament']);
    $GameInfo = strip_tags($_POST['gamenotes']);
    $GameLoc = $_POST['gamelocation'];
    if (isset($_POST['rulelist']))
        $RuleList = $_POST['rulelist'];   
    $Year = date("Y");
    if ($RuleList != "") {
        $DivisionList = $DivisionList ." - ".$RuleList;
    } else {
        $DivisionList = $DivisionList;
    }

    $SportName = $_REQUEST['sport'];
    $SportQry = $conn->prepare("SELECT * from sports where sport_name like '{$SportName}%'");
    $SportQry->execute();
    $SportCnt = $SportQry->rowCount();
    if ($SportCnt > 0) {
        $QrySportRow = $SportQry->fetchAll(PDO::FETCH_ASSOC);
        foreach ($QrySportRow  as $QrySportVal) {
            $SportId = $QrySportVal['sportcode']; 
        }
    }
    $SportId;

    $TeamvQuery = $conn->prepare("select * from teams_info where team_name =:visitor");
    $TeamvQuery->execute(array(":visitor"=>$Visitor));
    $CntTeamv = $TeamvQuery->rowCount();

    $TeamVid = '';
    if ($CntTeamv > 0) {
        $TeamvRows = $TeamvQuery->fetchAll(PDO::FETCH_ASSOC);
        foreach ($TeamvRows as $Vrow) {
            $Teamvid = $Vrow['id'];
            $Vcustid = $Vrow['customer_id'];
        }    
    } else {
        $Teamvid = "";
    }

    $TeamhQuery = $conn->prepare("select * from teams_info where team_name =:home");
    $TeamhQuery->execute(array(":home"=>$Home));
    $CntTeamh = $TeamhQuery->rowCount();

    $Teamhid = '';
    if ($CntTeamh > 0) {
        $TeamhRows = $TeamhQuery->fetchAll(PDO::FETCH_ASSOC);
        foreach ($TeamhRows as $Hrow) {
            $Teamhid = $Hrow['id'];
            $Hcustid = $Hrow['customer_id'];
        }    
    } else {
        $Teamhid = "";
    }

	$Gress = $conn->prepare("select * from games_info where (visitor_team_id=:teamvid or home_team_id=:teamhid  or  visitor_team_id=:teamhid or home_team_id=:teamvid) and date=:gamedate and time=:gametime and year=:year");
    $GresArr = array(":teamvid"=>$Teamvid, ":teamhid"=>$Teamhid, ":gamedate"=>$GameDate, ":gametime"=>$GameTime, ":year"=>$Year);
    $Gress->execute($GresArr);    
    $CntGress = $Gress->rowCount();
    $TempArray = array();
    if ($CntGress == 0) {
        if ($DivisionList !="" ) {

            if ($_SESSION['master'] != 1 ) {
                $DivisionQuery = $conn->prepare("select * from customer_division where id = :divisionlist OR (name=:divisionlist and custid=:cid)");
                $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $CntDivisionQuery = $DivisionQuery->rowCount();
            } else {
                $children = array($_SESSION['childrens']);
                $ids = $_SESSION['loginid'].",".join(',',$children);
                $DivisionQuery = $conn->prepare("select * from customer_division where id ='$DivisionList' OR (name='$DivisionList' and custid  in ($ids))");
                $DivisionQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $CntDivisionQuery = $DivisionQuery->rowCount();
            }

            if ($CntDivisionQuery > 0) {
                $DivRows = $DivisionQuery->fetchAll(PDO::FETCH_ASSOC);
                foreach ($DivRows as $Drow) {
                    $Divid = $Drow['id'];
                }            
            } else {
                $InsertDivQuery = $conn->prepare("INSERT INTO customer_division(name,custid) VALUES (:divisionlist,:cid)");
                $InsertDivQuery->execute(array(":divisionlist"=>$DivisionList, ":cid"=>$Cid));
                $Divid = $conn->lastInsertId();
            }
        } else {
            $Divid = "";
        }

        if ($SeasonList !="" ) {
            $SeasonQuery = $conn->prepare("select * from customer_season where id =:seasonlist OR (name=:seasonlist and custid=:cid)");
            $SeasonQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
            $CntSeasonQuery = $SeasonQuery->rowCount();
            if ($CntSeasonQuery > 0) {
                $SeasonRows = $SeasonQuery->fetchAll(PDO::FETCH_ASSOC);
                foreach ($SeasonRows as $Snrow) {
                    $Snid = $Snrow['id'];
                }            
            } else {
                $InsertDivQuery = $conn->prepare("INSERT INTO customer_season(name,custid) VALUES (:seasonlist,:cid)");
                $InsertDivQuery->execute(array(":seasonlist"=>$SeasonList, ":cid"=>$Cid));
                $Snid = $conn->lastInsertId();
            }
        } else {
            $Snid = "";
        }  
        if ($Gender == "1") {
            $Gen = "M";
        } else {
            $Gen = "F";
        } 

        $Ggid = $conn->prepare("select id from games_info order by id desc limit 0,1");
        $Ggid->execute();
        $Ggrow = $Ggid->fetch();    
        $Gg = $Ggrow['id'];$Gg++; 

        $Date = str_replace('/', '', $GameDate);
        $Xmlmatch = "BB_".$Teamvid."_".$Teamhid."_".$Date."_".$Gen; 
        $Xmlquery = $conn->prepare("select * from games_info where new_game_xml like '{$Xmlmatch}%'");
        $Countxml = $Xmlquery->rowCount();
        $NewCountxml= $Countxml +1;
        $Gs = '';
        if ($SportName == 'basketball') {
            $Gs = 'BB';
        }
        if ($SportName == 'football') {
            $Gs = 'FB';
        }
        if ($SportName == 'baseball' || $SportName == 'softball') {
            $Gs = 'BA';
        }
        $Xmlname = $Gs."_".$Teamvid."_".$Teamhid."_".$Date.$Gg.".xml";

        $InsertGame = $conn->prepare("INSERT INTO games_info(sport_id, game_name, date, time, year, zone, visitor_team_id, home_team_id, team_type, game_location, isLeagueGame, entrymode, tournament, game_notes, location, division, season, new_game_xml, home_customer_id, visitor_customer_id) VALUES (:sportid, :gamename, :gamedate, :gametime, :year, :zone, :teamvid, :teamhid, :gender, :gameloc, :gametype, :entrymode, :tournament, :gameinfo, :location, :divid, :snid, :xmlname, :hcustid, :vcustid)");
        $insetArr =  array(":sportid"=>$SportId, ":gamename"=>$GameName, ":gamedate"=>$GameDate, ":gametime"=>$GameTime, ":year"=>$Year, ":zone"=>$Zone, ":teamvid"=>$Teamvid, ":teamhid"=>$Teamhid, ":gender"=>$Gender, ":location"=>$Location, ":gametype"=>$GameType, ":entrymode"=>$EntryMode, ":tournament"=>$Tournament, ":gameinfo"=>$GameInfo, ":gameloc"=>$GameLoc, ":divid"=>$Divid, ":snid"=>$Snid, ":xmlname"=>$Xmlname, ":hcustid"=>$Hcustid, ":vcustid"=>$Vcustid);
        $InsertGame->execute($insetArr);        

        if($InsertGame){
            $TempArray['gamestatus']    =  "success";
			$TempArray['updatestatus']    =  "insert";
			echo json_encode($TempArray);
        }

    } else {        
       	
		$TempArray['gamestatus']    =  "gameexists";

		$FetchGames = $Gress->fetchAll(PDO::FETCH_ASSOC);
	
		foreach ($FetchGames as $GameRow) {
			$ReponseArray['GameID']  =  $GameRow['id'];	
			$ReponseArray['visitor'] =  json_decode(getTeamName($GameRow['visitor_team_id']), true);	
			$ReponseArray['home']  =  json_decode(getTeamName($GameRow['home_team_id']), true);
			$ReponseArray['gamedate']  =  $GameRow['date'];	
			$ReponseArray['time']  =  $GameRow['time'];	
			$TempArray['gamedetails'][]   = $ReponseArray;
			$Inc++;
		}
		echo json_encode($TempArray);       
    }
}
exit;
?>