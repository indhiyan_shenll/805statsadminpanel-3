<?php 
include_once('session_check.php'); 
include_once("connect.php");
include_once('common_functions.php');
include_once('usertype_check.php');

if (isset($_REQUEST["firstname"])) {

    $teamid = $_REQUEST["hiddenteamid"];
    $hidden_team_name = $_REQUEST["hiddenteamname"];
    $firstname = $_REQUEST["firstname"];
    $lastname = $_REQUEST["lastname"];
    $uniform_no = $_REQUEST["uniformno"];
    $position = $_REQUEST["position"];
    $gender = $_REQUEST["gender"];
    $height = $_REQUEST["height"];
    $weight = $_REQUEST["weight"];
    $dob = $_REQUEST["dob"];
    $age = $_REQUEST["age"];
    $school = $_REQUEST["school"];
    $grade = $_REQUEST["grade"];
    $prev_school = $_REQUEST["prevschool"];    
    $created_date = date("Y-m-d H:i:s");
    $sportid = $_SESSION['sportid'];
    $sportname = $_SESSION['sportname'];
    $gameid = $_REQUEST["hiddengameid"];

    // $GameQry = $conn->prepare("select * from game_details where id=:id");
    // $GameQryArr = array(":id"=>$gameid);
    // $GameQry->execute($GameQryArr);
    // $fetchRow = $GameQry->fetch(PDO::FETCH_ASSOC);
    // $gameid = $fetchRow["id"];
    // $season = $fetchRow["season"];
    // $date = explode("-",$fetchRow["date"]);
    // $year = $date[0];

    $select_player = $conn->prepare("SELECT * from player_info where firstname=:firstname and lastname=:lastname and customer_id=:customer_id and team_id=:team_id");
    $select_player_arr = array(":firstname"=>$firstname, ":lastname"=>$lastname, ":customer_id"=>$cid, ":team_id"=>$teamid);
    $select_player->execute($select_player_arr);
    $cntplayer = $select_player->rowCount();
    $player_info_arr = "";
    if ($cntplayer > 0) {
        $fetch_player = $select_player->fetchAll(PDO::FETCH_ASSOC);
        
        foreach ($fetch_player as $fetchplayerrow) {
            $fetchplayerrow["status"] = "playeralreadyexists";
            $player_info_arr[] = $fetchplayerrow;

        }
        
    } else {

        $insert_player = $conn->prepare("INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, height, weight, isActive, date_of_birth, sport_id, date_added) VALUES (:customer_id, :firstname, :lastname, :gender, :age, :school, :prev_school, :grade, :uniform_no, :team_id, :position, :height, :weight, :isActive, :dob, :sportid, :created_date)");    
        $insert_player_arr = array(":customer_id"=>$cid, ":firstname"=>$firstname, ":lastname"=>$lastname, ":gender"=>$gender, ":age"=>$age, ":school"=>$school, ":prev_school"=>$prev_school, ":grade"=>$grade, ":uniform_no"=>$uniform_no, ":team_id"=>$teamid, ":position"=>$position, ":height"=>$height, ":weight"=>$weight, ":isActive"=>"1", ":dob"=>$dob, ":sportid"=>$sportid, ":created_date"=>$created_date);
        $insertres = $insert_player->execute($insert_player_arr);
        $palyer_id = $conn->lastInsertId();
        if ($insertres) {

            $select_player = $conn->prepare("SELECT * from player_info where id=:id");
            $select_player_arr = array(":id"=>$palyer_id);
            $select_player->execute($select_player_arr);
            $fetch_player = $select_player->fetch(PDO::FETCH_ASSOC);

            // $checkname = $fetch_player["lastname"].", ".$fetch_player["firstname"];
            // $insertPlayer = $conn->prepare("INSERT INTO individual_player_stats(playercode, year, season, checkname, gamecode, teamcode, gp, gs, fgm, fga, fgm3, fga3, ftm, fta, tp, oreb, dreb, treb, pf, tf, ast, to1, blk, stl, min) VALUES (:playercode, :year, :season, :checkname, :gamecode, :teamcode, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min)");    
            // $insertPlayerArr = array(":playercode"=>$palyer_id, ":year"=>$year, ":season"=>$season, ":checkname"=>$checkname, ":gamecode"=>$gameid, ":teamcode"=>$fetch_player["team_id"], ":gp"=>0, ":gs"=>0, ":fgm"=>0, ":fga"=>0, ":fgm3"=>0, ":fga3"=>0, ":ftm"=>0, ":fta"=>0, ":tp"=>0, ":oreb"=>0, ":dreb"=>0, ":treb"=>0, ":pf"=>0, ":tf"=>0, ":ast"=>0, ":to1"=>0, ":blk"=>0, ":stl"=>0, ":min"=>0);
            // $insertres = $insertPlayer->execute($insertPlayerArr);
            // $palyer_stats_id = $conn->lastInsertId();

            $player_info_arr[0]["status"] = "success";
            $player_info_arr[0]["id"] = $fetch_player["id"];
            // $player_info_arr[0]["id"] = $palyer_stats_id;
            $player_info_arr[0]["lastname"] = $fetch_player["lastname"];
            $player_info_arr[0]["firstname"] = $fetch_player["firstname"];
            $player_info_arr[0]["teamname"] = $hidden_team_name;
        } else {
            $player_info_arr[0]["status"] = "Fail";
        }
    }
    echo json_encode($player_info_arr);
}
exit;
