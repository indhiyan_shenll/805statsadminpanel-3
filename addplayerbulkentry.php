<?php
include_once("connect.php");
// include_once('session_check.php');


if (isset($_POST['firstname']) && (!empty($_POST['firstname']))) {
	$firstname   =  $_POST['firstname'];
	$lastname    =  $_POST['lastname'];
	$gender      =  $_POST['gender'];
	$height      =  $_POST['plheight'];
	$weight      =  $_POST['plweight'];
	$dob         =  $_POST['pl_dob'];
	$age         =  $_POST['pl_age'];
	$school      =  $_POST['school'];
	$grade       =  $_POST['grade'];
	$prevschool  =  $_POST['prevschool'];
	$hometown    =  preg_replace('/\s+/', '', $_POST['hometown']);
	$uniform     =  $_POST['uniform'];
	$position    =  $_POST['position'];
	$team        =  $_POST['team'];
	$playernotes =  $_POST['playernotes'];
	$sportname   =  $_POST['sport'];	
	$cid         =  $_POST['cid'];
	$addplayer   =  $_POST['addplayer'];
	$playerid    =  $_POST['playerid'];
	
	$filename="";

	$sportquery = $conn->prepare("select * from sports where sport_name like '{$sportname}%'");
	$sportquery->execute();
	$num_rows = $sportquery->rowCount();
	// $num_rows = mysql_num_rows($sportquery);
	if ($num_rows>0){
		$FetchSport = $sportquery->fetchAll(PDO::FETCH_ASSOC);
		foreach($FetchSport as $sportids) {
			$sportid = $sportids['sportcode'];
		}
		// while($sportids=mysql_fetch_array($sportquery))
		// {
		// 	$sportid= $sportids['sportcode'];
		// }
	}

	if(!empty($_FILES["file"]["type"]))
	{
		$validextensions = array("jpeg", "jpg", "png");
	    $temporary = explode(".", $_FILES["file"]["name"]);
        if( $temporary!=""){
			$file_extension = end($temporary);
			
			$n=date("ymdihs");
			$sourcePath = $_FILES['file']['tmp_name']; 
			$targetPath =preg_replace('/\s+/', '',"uploads/players/".$n.$_FILES['file']['name']); 					

			$test = move_uploaded_file($sourcePath,$targetPath) ; 
			$filename=preg_replace('/\s+/', '',$n.$_FILES['file']['name']);	
		} else {
			$filename="";
		}
	} else {
			$filename="";
	}
	
	$teamquery = $conn->prepare("select * from teams_info where team_name ='$team' and customer_id='$cid'");
	$teamquery->execute();
	$CntTeamrows = $teamquery->rowCount();
	$teamid ="";
	$PlayerIds = '';

	if ($CntTeamrows>0){
		$FetchTeams = $teamquery->fetchAll(PDO::FETCH_ASSOC);
		foreach($FetchTeams as $trow) {
			$teamid =$trow['id'];
		}
	}
	// if (mysql_num_rows($teamquery)){
	// 	while($trow = mysql_fetch_assoc($teamquery)){
	// 		$teamid =$trow['id'];
	// 	}
	// }
	
	$playequery = $conn->prepare("select * from player_info where firstname ='$firstname' and lastname='$lastname' and team_id='$team'");
	$playequery->execute();
	$numrows = $playequery->rowCount();

	// $numrows = mysql_num_rows($playequery);
	
	if(($addplayer=='')&&($numrows==0)){
		$insertplayer = $conn->prepare("INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, hometown, player_note, height, weight, image, date_of_birth, sport_id) VALUES ('$cid', '$firstname', '$lastname', '$gender', '$age', '$school', '$prevschool', '$grade', '$uniform', '$team', '$position', '$hometown', '$playernotes', '$height', '$weight', '$filename',  '$dob', '$sportid')");
		$insertplayer->execute();	
		// mysql_query($insertplayer);
		
		$TempArray['playerstatus']    =  "success";
		$TempArray['updatestatus']    =  "insert";
		echo json_encode($TempArray);
		
	}else if($addplayer=='update'){
		$sql = $conn->prepare("UPDATE player_info SET firstname='$firstname', lastname='$lastname', sex='$gender', age='$age', school='$school', prev_school='$prevschool', grade='$grade', uniform_no='$uniform', team_id='$team', position='$position', hometown='$hometown', player_note='$playernotes',height='$height', weight='$weight',image='$filename', date_of_birth='$dob', sport_id='$sportid' WHERE id='$playerid'");
		$sql->execute();	
		// mysql_query($sql);

		$TempArray['playerstatus']    =  "success";
		$TempArray['updatestatus']    =  "update";
		echo json_encode($TempArray);
				
	}
	else if(($addplayer=='')&&($numrows>0)){
		$Inc = 0;
		$Plimgdisp = '';
		$TempArray = array();
		$TempArray['playerstatus']    =  "playerexits";

		$FetchPlayers = $playequery->fetchAll(PDO::FETCH_ASSOC);
		
		foreach ($FetchPlayers as $PlayerRow) {
			$ReponseArray['PlayerID']  =  $PlayerRow['id'];	
			$ReponseArray['PlayerImg'] =  ($PlayerRow['image']!='')?$PlayerRow['image']:'no-playerimg.png';			
			$ReponseArray['Firstname'] =  $PlayerRow['firstname'];	
			$ReponseArray['Lastname']  =  $PlayerRow['lastname'];
			$TempArray['playerdetails'][]   = $ReponseArray;
			$Inc++;
		}

		// while($PlayerRow = mysql_fetch_assoc($playequery)){
			
		// 	$ReponseArray['PlayerID']  =  $PlayerRow['id'];	
		// 	$ReponseArray['PlayerImg'] =  ($PlayerRow['image']!='')?$PlayerRow['image']:'no-playerimg.png';			
		// 	$ReponseArray['Firstname'] =  $PlayerRow['firstname'];	
		// 	$ReponseArray['Lastname']  =  $PlayerRow['lastname'];
		// 	$TempArray['playerdetails'][]   = $ReponseArray;
		// 	$Inc++;
		// }					
		echo json_encode($TempArray);
	}else{
		$insertplayer = $conn->prepare("INSERT INTO player_info(customer_id, firstname, lastname, sex, age, school, prev_school, grade, uniform_no, team_id, position, hometown, player_note, height, weight, image, date_of_birth, sport_id) VALUES ('$cid', '$firstname', '$lastname', '$gender', '$age', '$school', '$prevschool', '$grade', '$uniform', '$team', '$position', '$hometown', '$playernotes', '$height', '$weight', '$filename',  '$dob', '$sportid')");
		$insertplayer->execute();		
		// mysql_query($insertplayer);

		$TempArray['playerstatus']    =  "success";
		$TempArray['updatestatus']    =  "insert2";
		echo json_encode($TempArray);
	}
	exit;
}

?>