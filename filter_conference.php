<?php 
include_once('session_check.php'); 
include("connect.php");
 $customer_id=$_SESSION['loginid'];
if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	//$HdnMode=$_REQUEST["HdnMode"];
	//$HdnPage=$_REQUEST["HdnPage"];
	$Page=1;
	//$Page=1;
	$conferencename     =  $_REQUEST['conferencename'];
}
$sports[]=array();
$sportslists = "select * from customer_subscribed_sports where customer_id=:cid";
$sportslistsqry = $conn->prepare($sportslists);
$sportslistsqry->execute(array(":cid"=>$customer_id));
$soprts_Count = $sportslistsqry->rowCount();
if($soprts_Count>0){
    $getResSports     =   $sportslistsqry->fetch();
    foreach($getResSports as $sportlist)
    {
        $sports[]= $getResSports['sport_id']; 
    }
}
if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
      $getSportsRow=$get_sport_qry->fetch();
      $sportid= $getSportsRow['sportcode'];
    }
}
else{
	
    $sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
}
if($soprts_Count>0){
	if($sports[1]=='4444') { $ls='basketball'; } 
	if($sports[1]=='4443') { $ls='football'; } 
	if($sports[1]=='4441') { $ls='baseball'; } 
	if($sports[1]=='4442') { $ls='softball'; } 
}
?>
<form id="frm_confrerence_list" name="frm_confrerence_list" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<input type="hidden" name="hdnsearch" id="hdnsearch" value="<?php echo $conferencename; ?>">
<table class="table table-striped table-bordered table-hover dataTable no-footer dataTable" id="sample_1">
<thead>
	<tr>
		<th> Conference Id </th>
        <th> Conference Name </th>      
        <th> Actions </th>
	</tr>
</thead>
<tbody>
<?php

if(isset($_REQUEST['conferencename']))
{

	if ($_SESSION['master'] == 1) {
        $children = array($_SESSION['childrens']);
        $customer_id = $_SESSION['loginid'].",".join(',',$children);
    } else {
        $customer_id = $_SESSION['loginid'];
    }
	$conferencename     =  $_REQUEST['conferencename'];
	$res = '';
	if($conferencename != ''){
		$res = "select * from customer_conference where conference_name like '%$conferencename%' and customer_id in ($customer_id)";
	} else {
		$res = "select * from customer_conference where customer_id in ($customer_id)";
	}
    //echo $res;
    $getResQry      =   $conn->prepare($res);
    $QryArr = array(":organizationame"=>$conferencename, ":customer_id"=>"$customer_id");
    $getResQry->execute($QryArr);
    $getResCnt      =   $getResQry->rowCount();
    $getResQry->closeCursor();
    	$TotalPages = '';
	    if($getResCnt>0){
	        $TotalPages=ceil($getResCnt/$RecordsPerPage);
	        $Start=($Page-1)*$RecordsPerPage;
	        $sno=$Start+1;
	            
	        $res.=" limit $Start,$RecordsPerPage";
	                
	        $getResQry      =   $conn->prepare($res);
	        $getResQry->execute($QryArr);
	        $getResCnt      =   $getResQry->rowCount();
	    if($getResCnt>0){
	        $getResRows     =   $getResQry->fetchAll();
	        $getResQry->closeCursor();
	        $s=1;
        foreach($getResRows as $conference){
			$sportname=$sportname?$sportname:$ls;
		?>
			<tr>
                <td><?php echo $conference['id'] ?></td>
                <td nowrap><?php echo $conference['conference_name'] ?></td>
				<td>
					<?php 
					$division_id = $conference['id'];
					$ConfQry = $conn->prepare("select * from customer_conference where id='$division_id' and customer_id in ($customer_id)");
					$ConfQry->execute(); 
					$CntConf = $ConfQry->rowCount();
				    $ConfResRow = $ConfQry->fetchAll(PDO::FETCH_ASSOC);
						
					foreach($ConfResRow as $ConfRes) {
						
						$customer_conf_name =  explode(" - ",$ConfRes['name']);	$db_division_name =  $customer_conf_name[0];
						$db_division_rule =  $customer_conf_name[1];
					}				   
				   ?>				   
					<a href="#" id="edit_division" data-id="<?php  echo base64_encode($conference['id']); ?>" data-name="<?php echo $conference['conference_name'];?>"
                    data-sport="<?php echo $sportname; ?>" data-toggle="modal" class="btn btn-xs btn-success edit_popup"  data-cid="<?php echo $customer_id;?>" customerid="<?php echo $conference['id']; ?>"><i class="fa fa-pencil"></i> Edit
                    </a>												
								
                </td>
            </tr>

		<?php
		$s++;
		}
	} 
    else{
            // echo "<tr><td colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
        }
    }
    else{
        // echo "<tr><td  colspan='3' style='text-align:center;'>No Conference(s) found.</td></tr>";
    }
	
}?>
 </tbody>
 </table>
<?php
	if($TotalPages > 1){

	echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
	$FormName = "frm_confrerence_list";
	require_once ("paging.php");
	echo "</td></tr>";

	}
?>
</form>
<script src="assets/custom/js/manageconference.js" ></script>
<!-- <link href="assets/custom/css/conferencelist.css" rel="stylesheet" type="text/css" /> -->




