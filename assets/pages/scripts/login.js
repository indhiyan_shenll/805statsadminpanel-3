jQuery(document).ready(function() {

    //Specify validation rules for login form
    $('.login-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        rules: {
            email: {
                required: true,
                email: true
            },
            password: {
                required: true
            },
            remember: {
                required: false
            }
        },

        messages: {
            email: {
                required: "Email is required.",
                email: "Enter valid Email."
            },
            password: {
                required: "Password is required."
            }
        },

        // invalidHandler: function(event, validator) { //display error alert on form submit   
        //     // $('.alert-danger', $('.login-form')).show();
        //     $('.alert-danger').show();
        // },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            // error.insertAfter(element.closest('.input-icon'));
            error.insertAfter(element);
        },

        submitHandler: function(form) {            
            form.submit(); // form validation success, call ajax form submit
        }
    });

    jQuery('#forget-password').click(function() {
        jQuery('.login-form').hide();
        jQuery('.forget-form').show();
    });

    jQuery('#back-btn').click(function() {
        jQuery('.login-form').show();
        jQuery('.forget-form').hide();
    });

    //Specify validation rules for forget form
    $('.forget-form').validate({
        errorElement: 'span', //default input error message container
        errorClass: 'help-block error', // default input error message class
        focusInvalid: false, // do not focus the last invalid input
        ignore: "",
        rules: {
            forgetemail: {
                required: true,
                email: true
            }
        },

        messages: {
            forgetemail: {
                required: "Email is required.",
                email: "Enter valid Email."
            }
        },

        // invalidHandler: function(event, validator) { //display error alert on form submit   

        // },

        highlight: function(element) { // hightlight error inputs
            $(element)
                .closest('.form-group').addClass('has-error'); // set error class to the control group
        },

        success: function(label) {
            label.closest('.form-group').removeClass('has-error');
            label.remove();
        },

        errorPlacement: function(error, element) {
            // error.insertAfter(element.closest('.input-icon'));
            error.insertAfter(element);
        },

        submitHandler: function(form) {               
            form.submit();
        }
    });
});