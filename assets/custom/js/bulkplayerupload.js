
var PlayerExistChk  = false;

// jQuery(function($){
	
// 	$(".pl_dobmask").mask("99/99/9999");
// });

$(document).on('click','.addmoreplayerbtn',function(){
	var $this  = $(this);
	var AddPlayerHTML  = $('#addmoreplayercont').html();
	var FormEntryLength = $('#bultplayentrycont').length+1;	
	$('#bultplayentrycont').append(AddPlayerHTML);
	// $(".pl_dobmask").mask("99/99/9999");
});

$(document).on('click','.deleteplayerbtn',function(){
	var $this  = $(this);	
	if($('#bultplayentrycont form').length>1){
	 $('#bultplayentrycont form:last-child').remove();
	}else{
		alert('Minimum one row is required');
		return false;
	}
});


$(document).on('click','.submitallplayerbtn',function(){	
	
	var $thisbtn = $(this);

	var RegExpression = /^[a-zA-Z\s]*$/;  
	var AddPlayerChk = true;
	var FormCount  = $('#bultplayentrycont .multipleplayerformgrp').length;
	var Inc= 0;	
	
     var allvalid=true;
	
	$('#bultplayentrycont .multipleplayerformgrp').each(function(){	
		var formchk  = true;
		var $thisform = $(this);	
		$thisform.css('border','0px solid red');
		$thisform.find('input').css('border','1px solid #d6d6d6');
		$thisform.find('select').css('border','1px solid #d6d6d6');		
		$thisform.find(".playerimg").css('border','1px solid #d6d6d6');	

		var teaminput = $thisform.find("#teaminput").val();
		var firstname = $thisform.find("#firstname").val();
		var lastname  = $thisform.find("#lastname").val();
		var uniform   = $thisform.find("#uniform").val();
		var file      = $thisform.find("#file").val();
		
		if(file!=''){
			var filesize = $thisform.find("input:file")[0].files[0].size; 
			var FileExtension = file .replace(/^.*\./, '');
		}
		

		if(teaminput==''){
			$thisform.find("#teaminput").focus();
			$thisform.find("#teaminput").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		if(firstname==''){
			$thisform.find("#firstname").focus();
			$thisform.find("#firstname").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}else  if(!RegExpression.test(firstname)){
			$thisform.find("#firstname").focus();
			$thisform.find("#firstname").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		// }else if( firstname.indexOf(" ") !== -1 )
		// {			
		// 	$thisform.find("#firstname").focus();
		// 	$thisform.find("#firstname").css('border','1px solid red');
		// 	allvalid=false;
		// 	formchk  = false;
		// }
		if(lastname==''){
			$thisform.find("#lastname").focus();
			$thisform.find("#lastname").css('border','1px solid red');
			allvalid=false;
		}else if(!RegExpression.test(lastname)){
			$thisform.find("#lastname").focus();
			$thisform.find("#lastname").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}
		// else if( lastname.indexOf(" ") !== -1 )
		// {
		// 	$thisform.find("#lastname").focus();
		// 	$thisform.find("#lastname").css('border','1px solid red');
		// 	allvalid=false;
		// 	formchk  = false;
		// }
		if(uniform==''){
			$thisform.find("#uniform").focus();
			$thisform.find("#uniform").css('border','1px solid red');
			allvalid=false;
			formchk  = false;
		}else if(isNaN(uniform)){
			$thisform.find("#uniform").focus();
			$thisform.find("#uniform").css('border','1px solid red');
			allvalid=false;		
			formchk  = false;
		}
		if((file!='') && (filesize>300000) ){		
			$thisform.find(".playerimg").css('border','1px solid red');
			alert('File size should be less than 300kb');
			allvalid=false;
			formchk  = false;
		}else if((file!='') && (FileExtension!='png')&& (FileExtension!='jpeg')&& (FileExtension!='jpg')){
			$thisform.find(".playerimg").css('border','1px solid red');		
			allvalid=false;
			formchk  = false;		
		}
		
		if(!formchk)
		{
			$thisform.css('border','1px solid red');
		}else{
			$thisform.css('border','0px solid red');
		}

	});	
	if(!allvalid)
	{
		return false;
	}

	$thisbtn.hide();

	$('#bultplayentrycont .multipleplayerformgrp').each(function(){		
		 var $thisform = $(this);
		 var file_data = $thisform.find("#file").prop('files')[0];   
		 var formData = new FormData($(this)[0]);
		 $thisform.find('.uploadstatus>img').show();
		 $.ajax({
			type : "POST",
			url : "addplayerbulkentry.php",
			data : formData,	
			async:false,
			contentType: false,
			processData: false,
			success : function(response) {
				var response = $.parseJSON(response);
				console.log(response.playerstatus);				
				if(response.playerstatus=='playerexits'){					
					var PlayersLength  =  response.playerdetails;
					var PlayersHtml    = '';
					var PlayerCode = '';
					for(var i=0;i<PlayersLength.length;i++ ){
						var PlayersFirstname = PlayersLength[i].Firstname;
						var PlayersLastname  = PlayersLength[i].Lastname;
						var PlayersImage     = PlayersLength[i].PlayerImg;
						var PlayerID         = PlayersLength[i].PlayerID;

						PlayersHtml +='<div class="col-md-12" style="border: 1px solid #CCC;padding-bottom: 10px;    padding-top: 10px;"><p>This Player already exist with Player ID:<span id="playerid"><b>'+PlayerID+'</b></span></p><img src="uploads/players/'+PlayersImage+'" alt="" width="50" height="50" style="float:left;margin-top: 15px;"><div class="col-md-3" style="padding-top: 10px;">First Name:<b> '+PlayersFirstname+'</b></div><div class="col-md-3" style="padding-top: 10px;">Last Name:<b> '+PlayersLastname+'</b></div><div class="col-md-3"><button type="button" class="btn btn_purple updateplayer" style="margin-top: 15px;" id="updateplayer">Update this player Info</button></div></div>';
						PlayerCode += PlayerID;
					}

					 PlayersHtml = '<div class="popupplayercont" style="display:none;">'+PlayersHtml+'<button type="button" class="btn btn-success addnewplayer" style="margin-top: 15px;font-size: 13px;" id="addnewplayer">Add as New Player</button><div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';

					$thisform.find('.uploadstatus').empty().append('<img src="images/editplayer.png" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg exitsmodelshow" playerid="'+PlayerCode+'">'+PlayersHtml);	
					$thisform.css('border','1px solid red');
					$thisform.addClass('exitsplayerform');
					PlayerExistChk= true;
				}else if(response.playerstatus=='success'){
					// console.log(response.playerdetails);
					$thisform.find('input, textarea, button, select').attr('disabled','disabled');
					$thisform.find('.uploadstatus').empty().append('<img src="images/yes.gif" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg">');	
				}				
			},
			error: function(jqXHR, textStatus, errorThrown){
				 alert(textStatus, errorThrown);
			}
		});	
	});
		
});


$('.playerimg').bind('change', function() {

	if (this.files[0].size>300000) {
			alert('File size should be less than 300kb');
			return false;
	}else{
		return true;		
	}
});


$(document).on('click','.exitsmodelshow',function(){
	var $this    = $(this);
	var PlayerID = $this.attr('playerid');
	var BtnsHtml ='';
		
		var $thisform  = $(this).closest('.multipleplayerformgrp');
		$('#updatemsg').hide();
		var formattrid  = $thisform.index();

		$thisform.attr("form-id","100"+formattrid);
		
	if(PlayerID!=''){
		$('.updateplayerform').attr("form-id","100"+formattrid);
		$this.closest('#addplayermaincont').find('.updateplayerform #playerid').text(PlayerID); 
		$('#updatemsg').hide();
		
		var ExitsPlayerHTML = $this.next('.popupplayercont').html();

		$('.updateplayerform').modal('show');		
		$this.closest('#addplayermaincont').find('.updateplayerform .modal-body').empty();
		BtnsHtml +='<button type="button" class="btn btn-success addnewplayer" id="addnewplayer">Add as New Player</button><div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';
		$this.closest('#addplayermaincont').find('.updateplayerform .modal-body').append(ExitsPlayerHTML);
		$this.closest('#addplayermaincont').find('.updateplayerform .uploadcont').show();

	}else{		

		$('.updateplayerform .modal-body').empty();
		$('.updateplayerform').attr("form-id","100"+formattrid);

		$('.updateplayerform').modal('show');	
		BtnsHtml +='<div class="uploadcont"><p>This Player not exist</p><button type="button" class="btn btn-success addnewplayer" id="addnewplayer">Add as New Player</button></div>';

		BtnsHtml +='<div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';

		$this.closest('#addplayermaincont').find('.updateplayerform .modal-body').append(BtnsHtml);
		$this.closest('#addplayermaincont').find('.updateplayerform .uploadcont').show();
	}
});


$(document).on('click','.updateplayer,.addnewplayer',function(){
	var $this    = $(this);
	var $thisaddchk = $this.attr('id');
	var PlayerID = $this.closest('.col-md-12').find('#playerid>b').text();	
	

	if($thisaddchk=='updateplayer'){		
		var $thisform= $this.closest('#addplayermaincont')
							.find("#bultplayentrycont .uploadstatusimg[playerid*='"+PlayerID+"']")
							.closest('form');

		$thisform.find('[name="playerid"]').val(PlayerID);

		if($thisaddchk=='updateplayer'){
			$thisform.find('[name="addplayer"]').val('update');
		}else{
			$thisform.find('[name="addplayer"]').val('add');
		}
	}else{
		var formid  = $this.closest('.updateplayerform').attr('form-id');

		var $thisform= $this.closest('#addplayermaincont')
							.find('#bultplayentrycont .exitsplayerform[form-id="'+formid+'"]');
							

		$thisform.find('[name="addplayer"]').val('add');
		$thisform.removeClass('exitsplayerform');
	}
	$('.uploadcont').hide();
	$this.closest('.modal-body').find('.loadingimgcont').show();
	$this.closest('.modal-body').find('#updatemsg').hide();
	$this.closest('.modal-body').find('.col-md-12,.addnewplayer').hide();	
	$this.closest('.modal-body').find('.loadingimg').show();	
	var file_data = $thisform.find("#file").prop('files')[0];   
	var formData = new FormData($thisform[0]);

	$.ajax({
		type : "POST",
		url : "addplayerbulkentry.php",
		data : formData,	
		contentType: false,
		processData: false,		
		success : function(response) {
		console.log(response);	
		var response = $.parseJSON(response);
		if(response.playerstatus=='success'){				
			if($thisaddchk=='updateplayer'){
				$this.closest('.modal-body').empty().html('<div class="alert alert-success" id="updatemsg" style="display:none;">Player details updated successfully</div>');	
			}else{
				$this.closest('.modal-body').empty().append('<div class="alert alert-success" id="updatemsg" style="display:none;">Player details inserted successfully</div>');	
			}			
			$('.modal-body').find('#updatemsg').show();	
			$thisform.find('.exitsmodelshow').removeClass('exitsmodelshow').attr('src','images/yes.gif');
			$thisform.removeClass('exitsplayerform');
			$thisform.find('input, textarea, button, select').attr('disabled','disabled');
			$thisform.css('border','0px solid red');
		}
		},
		error:function(data){
			alert(data);
		}
	});		
	
});
//onchange="displayPreview(this.files)"

var _URL = window.URL || window.webkitURL;
function displayPreview(files) {
   var file = files[0]
   var img = new Image();
   var sizeKB = file.size / 1024;
   img.onload = function() {      
      alert("Size: " + sizeKB + "KB\nWidth: " + img.width + "\nHeight: " + img.height);
   }
   img.src = _URL.createObjectURL(file);
}



$(document).on('blur',".plnew_firstname,.plnew_lastname,.uniformint", function() {
		
	var RegExpression = /^[a-zA-Z\s']*$/;  
    var allvalid=true;	
	var name = $(this).val();
	var $thisform  =$(this).closest('form');	
		
	  if($thisform.hasClass('exitsplayerform')){

		var uniformvalchk = $(this).attr('id');

		if(name==''){
			$(this).focus();
			$(this).css('border','1px solid red');
			allvalid= false;
		}else if((uniformvalchk!='uniform') && (!RegExpression.test(name))){
			$(this).focus();
			$(this).css('border','1px solid red');
			allvalid= false;
		}else if((uniformvalchk=='uniform')&&(isNaN(name))){			
			$(this).focus();
			$(this).css('border','1px solid red');
			allvalid= false;		
		}

		if(!allvalid){
			return false;
		}		
		$(this).css('border','1px solid #d6d6d6');

		var formData = new FormData($thisform[0]);

		if(PlayerExistChk){
			$.ajax({
				type : "POST",
				url : "playerexitscheck.php",
				data : formData,	
				async:false,
				contentType: false,
				processData: false,
				success : function(response) {
					var response = $.parseJSON(response);

					if(response.playerstatus=='playerexits'){					
						var PlayersLength  =  response.playerdetails;
						var PlayersHtml    = '';
						var PlayerCode = '';
						for(var i=0;i<PlayersLength.length;i++ ){
							var PlayersFirstname = PlayersLength[i].Firstname;
							var PlayersLastname  = PlayersLength[i].Lastname;
							var PlayersImage     = PlayersLength[i].PlayerImg;
							var PlayerID         = PlayersLength[i].PlayerID;

							PlayersHtml +='<div class="col-md-12" style="border: 1px solid #CCC;padding-bottom: 10px;    padding-top: 10px;"><p>This Player already exist with Player ID:<span id="playerid"><b>'+PlayerID+'</b></span></p><img src="uploads/players/'+PlayersImage+'" alt="" width="50" height="50" style="float:left;"><div class="col-md-3" style="padding-top: 10px;">First Name:<b> '+PlayersFirstname+'</b></div><div class="col-md-3" style="padding-top: 10px;">Last Name:<b> '+PlayersLastname+'</b></div><div class="col-md-3"><button type="button" class="btn btn-primary updateplayer" id="updateplayer">Update this player Info</button></div></div>';
							PlayerCode += PlayerID;
						}

						 PlayersHtml = '<div class="popupplayercont" style="display:none;">'+PlayersHtml+'<button type="button" class="btn btn-success addnewplayer" id="addnewplayer">Add as New Player</button><div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';

						$thisform.find('.uploadstatus').empty().append('<img src="images/editplayer.png" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg exitsmodelshow" playerid="'+PlayerCode+'">'+PlayersHtml);	
						$thisform.css('border','1px solid red');
						$thisform.addClass('exitsplayerform');
						PlayerExistChk= true;
					}else if(response.playerstatus=='success'){
						console.log(response.playerdetails);
						$thisform.find('input, textarea, button, select').attr('disabled','disabled');
						PlayersHtml = '<p>Player not exist with this name</p><button type="button" class="btn btn-success addnewplayer" id="addnewplayer">Add as New Player</button><div class="loadingimgcont"><img src="images/playerloading.gif" style="margin:auto; width: 80px;" class="loadingimg"><div class="alert alert-success" id="updatemsg" style="display: none;">Details updated successfully</div></div>';
						$thisform.find('.popupplayercont').empty().append(PlayersHtml);
						//$thisform.find('.uploadstatus').empty().append('<img src="images/yes.gif" style="width: 16px; height: 16px; display: inline;" class="uploadstatusimg">');	
					}
				}
			});		
		}
	  }
	});



