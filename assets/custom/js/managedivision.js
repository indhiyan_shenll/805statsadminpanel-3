$(document).ready(function() {
	 $( ".resetbtn" ).click(function() {
        $( "#manage_division_search" ).val('');
        Managedivision();
    });


    $('.dt-buttons').hide();
	$('#loadingadds').hide();
	$('#divisionstsmsg').hide();
	
    $(document).on("click", "#edit_division", function () {       
	
        var customerid = $(this).attr('customerid');
		    $('#cust_divsion_id').val(customerid);
        //$('#weburl').val(weburl);

    });
//EditDivisionModal
	$(document).on("click", ".edit_popup", function () {       	
        var customerid = $(this).attr('customerid');
		$('#cust_divsion_id').val(customerid);
		var data_id=$(this).attr('data-id');
		var data_name=$(this).attr('data-name');
		var data_role=$(this).attr('data-role');
		var data_sports=$(this).attr('data-sports');
		 $('#division_name1').val(data_name);
		 $('#role_name').val(data_role);
		 $('#role_sports').val(data_sports);
		 $("#rulelist").val(data_role);
		 $( "#EditDivisionModal" ).modal("show");
		

    });

  /*$( "#manage_division_search" ).keyup(function() {
       var OrganName = $("#manage_division_search").val();
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
		
		 $.ajax({
			
            url:"filter_divisions.php",  
            method:'POST',
            data:{searchbyorganization: OrganName, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
            success:function(data) {
                // console.log(data);
				$(".loadingsection").hide();
                $('.customerlistparent').html(''); 
                $('.customerlistparent').html(data);
            }
          }); 
    });*/
	
	function managedivisionsearch(){
		var OrganName = $("#manage_division_search").val();
		
        var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
		
		 $.ajax({
				
            url:"filter_divisions.php",  
            method:'POST',
            data:{searchbyorganization: OrganName, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
            success:function(data) {
                // console.log(data);
			
                $('.customerlistparent').html(''); 
                $('.customerlistparent').html(data);
				$(".loadingsection").hide();
				$('.customerlistparent').show();
            }
          }); 
	}
    $( "#manage_division_search" ).keyup(function() {
		$(".loadingsection").show();
        $('.customerlistparent').hide();
        managedivisionsearch();
    });

function Managedivision(){
	 var OrganName = $("#manage_division_search").val();
	 var HdnPage        = $("#HdnPage").val();
        var HdnMode        = $("#HdnMode").val();
        var RecordsPerPage = $("#RecordsPerPage").val();
         $.ajax({
            url:"filter_divisions.php",  
            method:'POST',
            data:{searchbyorganization: OrganName, HdnPage: HdnPage, HdnMode: HdnMode, PerPage: RecordsPerPage},
            success:function(data) {
                // console.log(data);
                $('.customerlistparent').html(''); 
                $('.customerlistparent').html(data);
            }
          }); 
}

$('.addnewdivibtn').on('click', function(evt) {
	if (!$("#divisionfrm").validate()) { 
		return false;
	} 	
	$("#divisionfrm").submit();		
});


/*Edit */
$('.editnewdivibtn').on('click', function(evt) {
	if (!$("#editdivisionfrm").validate()) { 
		return false;
	} 	
	$("#editdivisionfrm").submit();		
});






});
//  Add & Edit modal popup
$(document).ready(function() {
$("#divisionfrm").validate({
        rules: {
          division_name:{required:true},
		  division_rulelist:{required:true,unique: true},
		  
         },
        messages: {
             division_name:{required:"Please enter division name"},
			 division_rulelist:{required:"Please select division rule",unique:"Division name already exists"},
        },

    });
		
	var response; 
	 $.validator.addMethod('unique',function(value){
		var customer_id=$('input[name="division_customer_id"]').val();
		var post_type="division_name_check";
		var divisionname=$("#division_name").val();
		var divisionrulelist=$('#rulelist1').val();
		
        $.ajax({
            method: "POST",
            url: "managedivision-ajax.php",
            async:false,
            data:{"post_type":post_type,"division_name":divisionname,"division_rulelist":divisionrulelist,"customer_id":customer_id},
      
            }).success(function(msg) {
                response = msg;                   
                
            });
	
            if(response == "not-exists")
                return true;
            else

                return false;    
    });
$("#editdivisionfrm").validate({
	 rules: {
		 division_name1:{required:true},
		 division_rulelist1:{required:true,requiredunique: true},
		 },
		messages: {
		 division_name1:{required:"Please enter division name"},
		 division_rulelist1:{required:"Please select division rule",requiredunique:"Division name already exists"},
	   },
		
	});
	var response; 
	 $.validator.addMethod('requiredunique',function(value){
		var customer_id=$('input[name="division_customer_id"]').val();
        var post_type="division_name_check";
		var divisionname=$("#division_name1").val();
		var divisionrulelist=$('#rulelist').val();
		var division_id=$('input[name="cust_divsion_id"]').val();
		   $.ajax({
            method: "POST",
            url: "managedivision-ajax.php",
            async:false,
            data:{"post_type":post_type,"division_name":divisionname,"division_rulelist":divisionrulelist,"customer_id":customer_id,"division_id":division_id},
      
            }).success(function(msg) {
                response = msg;                   
                
            });
	
            if(response == "not-exists")
                return true;
            else
                return false;    
    });

});
 $(document).ready(function() {
     $('#divisionlistingtable').DataTable({
            "retrieve": true,
            "paging": false,
            "bInfo": false,
           "bFilter":false,
            "bLengthChange":false,
            "bPaginate":false,
            "aaSorting": [[0,'desc']],
            "aoColumnDefs": [ { "bSortable": false, "aTargets": [2] } ], 
    });
});