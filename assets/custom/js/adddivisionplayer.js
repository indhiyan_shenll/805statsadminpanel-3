
jQuery(document).ready(function($) {
    $('#multiselect').multiselect({submitAllRight:false, });
});

jQuery(document).ready(function($) {

	$(document).on('click','.switchteambtn', function(evt) {
		var SwitchteamId	= $("#switchteamid").val();		
		var PlayerId	    = $("#playeridhid").val();	
		var ExsistTeamId    = $("#playerexsistidhid").val();	

		if(SwitchteamId==''){			
			$("#switchteamid-error").show();
			return false;
		}else{
			
			$("#loadingswitchtam").show();
			$("#teamswitchmsg, #switchteamform").hide();


			$.ajax({
			 type:"POST",
			 async:false,
			 url:"switchteam.php",
			 data:{teamid:SwitchteamId,playerid:PlayerId,exsistteamid:ExsistTeamId},
			 dataType:"text",
			 success: function(data) {				
				if(data=='success'){
					$("#loadingswitchtam").hide();
					$("#teamswitchmsg").show();
					$("#player_"+PlayerId).remove();
				}
			},
			error: function(data) {
				
			},
		 });	
		
		}

	});

	$(document).on('change','#switchteamid', function(evt) {
		if($(this).val()==''){
			$("#switchteamid-error").show();
			return false;
		}else{
			$("#switchteamid-error").hide();
			return false;
		}
	});

	
});

$(document).on("click",'.switchteam',function(){
	$("#loadingswitchtam").hide();
	$("#teamswitchmsg").hide();
	$("#switchteamform").show();
	$("#switchteamid-error").hide();
	$("#playeridhid").val($(this).attr("data-playerid"));
	$("#playerexsistidhid").val($(this).attr("teamid"));
	var SelectedTeamId = $("#undo_redo").val();
	var SelectedHtml   = $("#undo_redo").html();
	//$("#switchteamid").empty().append("<option value=''>Select Team</option>"+SelectedHtml);	
	$("#switchteamid option").attr("disabled",false);
	$("#switchteamid option[value=" + SelectedTeamId + "]").attr("disabled",true);
	//$("#switchteamid option[value=" + SelectedTeamId + "]").attr("selected",'selected');
	$("#SwitchTeamModal").modal("show");
});


$(document).on("click",'.addnewplayerbtn',function(){ 
	$("#multiselect_to option").prop("selected", "selected");
	var TeamId			= $("#teamidhidden").val();
	var SeasonId		= $("#seasonlist").val();
	var ConferenceId	= $("#conferencelist").val();
	var DivisionId		= $("#divisionlist").val();
	var SelectedPlayer	= $("#multiselect_to").val();
	$("#PlayerModal .modal-dialog").animate({width: "40%"}, 500 );

	if(SelectedPlayer==''){
		alert("Please add player");
		return false;
	}
	$("#loadingplayer").show();
	$(".formcontainer").hide();

	var result=$.ajax({
		 type:"POST",		
		 url:"saveplayerlist.php",
		 data:{"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId,PlayersId:SelectedPlayer}, 
		 success: function(data) {
			$("#undo_redo_to").empty().append(data);
			$("#loadingplayer").hide();
			$("#playerstsmsg").show();
		},
		error: function(data) {
			alert("something wrong");
		},
	 });	
			
	
});

$(document).on("click",'.addplayerbtntop',function(){ 
	var TeamId		 = $('#undo_redo').val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	var post_type    = 'selectplayeroption';

	$("#PlayerModal .modal-dialog").animate({width: "70%"}, 300 );

	if(SeasonId==''){
		alert("Please select seasaon");
		return false;
	}else if(ConferenceId==''){
		alert("Please select conference");
		return false;
	}else if(DivisionId==''){
		alert("Please select division");
		return false;
	}else if((TeamId=='')||(TeamId==null)){
		alert("Please select team");
		return false;
	}

	$("#loadingplayer,#playerstsmsg").hide();
	$(".formcontainer").show();
	
	var result=$.ajax({
		 type:"POST",
		 async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 dataType:"text",
		 success: function(data) {
			$("#multiselect_to").empty().append(data);		
			$("#PlayerModal").modal("show");
		},
		error: function(data) {
			alert("something wrong");
		},
	 });
		
});
$(document).on("change",'#undo_redo',function(){ 
    var TeamId		 = $(this).val();
	var SeasonId	 = $("#seasonlist").val();
	var ConferenceId = $("#conferencelist").val();
	var DivisionId	 = $("#divisionlist").val();
	
	if (!$("#addplayerform").validate()) { 		
		return false;
	} 	
	$("#undo_redo_to").empty().append("<img src='images/loading-publish.gif'>");

	var post_type='selectplayertbl';

	var result=$.ajax({
		 type:"POST",
		 //async:false,
		 url:"selectplayerlist.php",
		 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId,teamid:TeamId},
		 
		 success: function(data) {
		   var DataArr = [];		
		   DataArr = data.split('#####');
		  
		   $("#undo_redo_to").empty().append(DataArr[0]);
		   $("#PlayerModal .modal-body").empty().append(DataArr[1]);
		 },
		 error: function(data) {
			alert("something wrong");
			
		 },
	 }); 	
		
	});


$(document).ready(function() {
	$('#addplayer').multiselect({
		search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
        },
	});

	$('#undo_redo').multiselect({
		sort:false,
        search: {
            left: '<input type="text" name="q" class="form-control searchteambox" placeholder="Search Team" /><label>Select Team</label>',
            right: '<p class="clearfix" style="margin-bottom: 3px;margin-top: 0px;"><a href="add_divisionteam.php"><button type="button" class="btn uppercase pull-left backbtnred">Back</button></a><button type="button" class="btn uppercase addplayerbtntop" style="float: right;">Add Player</button></p><p class="clearfix" style="    margin-top: 0px;margin-bottom: 0px;"><label>Selected Player</label></p>',
        },
		moveToRight:function($left, $right, $options) { skipStack:false;},
		moveToLeft: function($left, $right, $options) {skipStack:false; }
    });


});

$(document).on('change','#seasonlist,#conferencelist,#divisionlist', function(evt) {
		var $this          =  $(this);	
		var SelectAttrid   =  $this.attr("id");
		var post_type      =  '';
		var SeasonId       =  '';	
		var ConferenceId   =  '';
		var DivisionId     =  '';

		if(SelectAttrid=="seasonlist"){
			if($(this).val()!=''){
				post_type      =  "seasonlist";
				SeasonId       =  $('#seasonlist').val();		
			}
		}else if(SelectAttrid=="conferencelist"){
			post_type      =  "conferencelist";
			SeasonId       =  $('#seasonlist').val();
			ConferenceId   =  $('#conferencelist').val();
			
		}else if(SelectAttrid=="divisionlist"){
			post_type      =  "divisionlist";
			SeasonId       =  $('#seasonlist').val();	
			ConferenceId   =  $('#conferencelist').val();
			DivisionId     =  $('#divisionlist').val();			
		}
		
		if(SeasonId!=''){
			var result=$.ajax({
				 type:"POST",				 
				 url:"selectteamlist.php",
				 data:{"post_type":post_type,"divisionid":DivisionId,"conferenceid":ConferenceId,"seasonid":SeasonId},		 
				 success: function(data) {
                      var responseArr = data.split("##^^##");
				   if(post_type=="seasonlist"){	
				   	   if(responseArr[1]=="failed"){
				         $('#undo_redo').empty();
				         $('.assignplayertbl').hide();
				         $('#conferencelist').empty().append(result.responseText);
						 $('#divisionlist').empty().append("<option value=''>Select</option>");
				        } else{				
						$('#conferencelist').empty().append(result.responseText);
						$('#divisionlist').empty().append("<option value=''>Select</option>");

					    }
					}else if(post_type=="conferencelist"){
					    if(responseArr[1]=="failed"){
						    $('#undo_redo').empty();
						    $('.assignplayertbl').hide();					
							$('#divisionlist').empty().append(result.responseText);
					    } else {
					     	$('#divisionlist').empty().append(result.responseText);	
					    }
						
					}else{
						$('.addteammainwrap').empty().append(result.responseText);
					}	
				 },
				 error: function(data) {
					alert("something wrong");
					
				 },
			 }); 			
		}
	
});


$(document).on('click','.removeplayerteam', function(evt) {
	if(!confirm("Are you sure want remove this player?")){
		return false;	
	}else{
		var $this = $(this);
		var PlayerId  = $(this).attr("date-playerid");
		var TeamId     = $(this).attr("teamid");
		$.ajax({
			 type:"POST",				 
			 url:"removeassignedplayer.php",
			 data:{"playerid":PlayerId,"teamid":TeamId},				 
			 success: function(data) {
				$this.closest("tr").remove();
				return false;
			 },
			 error: function(data) {
				alert("something wrong");
				return false;
			 },
		 }); 
	}
});


$(document).on("click",".updateplayer",function(){
    
    var selectedOption=$("#undo_redo").val();
	var selectedhtml =$("#undo_redo").html();
    var PlayerId  = $(this).attr("data-playerid");
    var teamid     = $(this).attr("teamid");
    var seasionid  = $(this).attr("seasionid");
    var divisionid  = $(this).attr("divisionid");
    var conferenceid  = $(this).attr("conferenceid");
    if(PlayerId!=""){
        $.ajax({
            url:"updateswitch_playerinfo.php",  
            method:'POST',
            data:{PlayerId:PlayerId,post_type:'updateswitch'},
            success:function(data) {
             var DataArr  = data.split("##^^##");
	            if(DataArr[0]=='Success'){ 
	              	var uniform_no=DataArr[1];
	              	var postion=DataArr[2];
	              	var postion_val=DataArr[3];
	              	var image_url=DataArr[4];
					$("#uniform_no").val(uniform_no);
					$("#position").html(postion);
					$("#hnd_player_id").val(PlayerId);
					$("#hnd_teamid").val(teamid);
					$("#hnd_seasionid").val(seasionid);
					$("#hnd_divisionid").val(divisionid);
					$("#hnd_conferenceid").val(conferenceid);
					$("#sel_switch").empty().html(selectedhtml);
					$('#sel_switch').prop('disabled', true);
					$("#hnd_image").val(image_url);
                    $("#sel_switch").val(selectedOption);
					$("#SwitchUpdateModel").modal("show");
	            } 
	            else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close" aria-hidden="true">&times;</a><strong>Something wrong</strong>');
                }
	        }
          });
    }
	
});
$(document).on("click",".switch_btn",function(){
	var $this  = $(this);
	var form_data	   =   $("#frm_switchteam").serialize();
	var player_id 	   =   $('input[name="hnd_player_id"]').val();
	var uniform_no 	   =   $('input[name="uniform_no"]').val();
	var postion 	   =   $('#postion').val();
	var upload_file_db  =	$('input[name="hnd_image"]').val();
	var info_form_data =   new FormData();
	var info_file_data =   $('#upload_file').prop('files')[0]; 
	info_form_data.append('info_file', info_file_data);
	info_form_data.append('form_data', form_data);
	info_form_data.append('post_type', 'manage_upadte_switch');
	info_form_data.append('upload_file_db', upload_file_db);
	info_form_data.append('player_id', player_id);
	$(".switchupdateplayer").show();
	$("#playerswichmsg").hide();
	$(".playerswichmsg").css("display","none");
	var regex = /^[0-9\b]+$/; 
	if(uniform_no==''){			
		$("#uniformnoid-error").show();
		return false;
	} else if(!regex.test(uniform_no)){
		$("#uniformid-error").show();
		return false;
	}
	else if(postion==''){
		$("#uniformnoid-error").hide();
		$("#positionid-error").show();
			return false;
	} else {
		$("#uniformnoid-error").hide();
		$("#uniformnid-error").hide();
		$("#positionid-error").hide();
	}
	jQuery.ajax({
			url:'updateswitch_playerinfo.php',
			type: "post",
			cache: false,
			contentType: false,
			processData: false,
			data: info_form_data,                         
			async:false,
			success: function(response){
				//alert(response);
				var responseArr = response.split("###");
				if(responseArr[0] == "success"){
					$("#player_"+responseArr[2]).find(".playerimgtbl").empty().append(responseArr[1]);
					$("#loadingplayers").hide();
		            $("#playerswichmsg").show();
		            $(".switchupdateplayer").hide();
		            $("#frm_switchteam")[0].reset();
		            if(responseArr[3]=="0"){
		            	$("#player_"+responseArr[2]).remove();
		            }
                } else {
                	$('.wrongstatus').empty().removeClass("alert-failure alert-success").addClass("alert-danger").show().html('<a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a><strong>Something wrong</strong>');
                }
			},
			error:function(){
				$("#frm_switchteam")[0].reset();
			}                
		});       
	

});
$(document).on("click",".btn_close",function(){
   $("#playerswichmsg").hide();
   $(".switchupdateplayer").show();
});

$(document).on('change','#uniform_no', function(evt) {
	    var regex = /^[0-9\b]+$/; 
	    var uniform=$(this).val();
		if($(this).val()==''){
			$("#uniformnoid-error").show();
			$("#uniformid-error").hide();
			return false;
		} else if (!regex.test(uniform)){
			$("#uniformnoid-error").hide();
			$("#uniformid-error").show();
		    return false;
		}else{
			$("#uniformnoid-error").hide();
			$("#uniformid-error").hide();
			return false;
		}
});
$(document).on('change','#position', function(evt) {

		if($(this).val()==''){
			$("#positionid-error").show();
			return false;
		}else{
			$("#positionid-error").hide();
			return false;
		}
});

   