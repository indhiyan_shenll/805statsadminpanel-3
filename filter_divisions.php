<?php 
include_once('session_check.php'); 
include("connect.php");
 $customer_id=$_SESSION['loginid'];
	if($_SESSION['childrens']){
		$childrens_id=$_SESSION['childrens'];
		$children = array($_SESSION['childrens']);
		array_push($children,$customer_id);
		$ids = join(',',$children); 

	}else{
		$ids=$customer_id;
	}
// error_reporting(0);
if(isset($_REQUEST["HdnMode"])){
	$RecordsPerPage=$_REQUEST["PerPage"];
	
	$HdnMode=$_REQUEST["HdnMode"];
	$HdnPage=$_REQUEST["HdnPage"];
	
	//$Page=$HdnMode;
	//$Page=$HdnPage;
	$Page=1;
	$searchname =  $_REQUEST['searchbyorganization'];
}
$sports[]=array();
$sportslists = "select * from customer_subscribed_sports where customer_id=:cid";
$sportslistsqry = $conn->prepare($sportslists);
$sportslistsqry->execute(array(":cid"=>$customer_id));
$soprts_Count = $sportslistsqry->rowCount();
if($soprts_Count>0){
    $getResSports     =   $sportslistsqry->fetch();
    foreach($getResSports as $sportlist)
    {
        $sports[]= $getResSports['sport_id']; 
    }
}
if(isset($_GET['sport'])){
   $sportname= $_GET['sport'];
   $sport_qry_str = "select * from sports where sport_name like :sportname";
   $get_sport_qry = $conn->prepare($sport_qry_str);
   $get_sport_qry->execute(array(":sportname"=>$sportname."%"));
   $get_soprts_Count = $get_sport_qry->rowCount();
   if($get_soprts_Count>0){
      $getSportsRow=$get_sport_qry->fetch();
      $sportid= $getSportsRow['sportcode'];
    }
}
else{
	
    $sportid= "4441' OR sport_id='4442' OR sport_id='4443' OR sport_id='4444' OR sport_id='4445' OR sport_id='4446" ;
}
if($soprts_Count>0){
	if($sports[1]=='4444') { $ls='basketball'; } 
	if($sports[1]=='4443') { $ls='football'; } 
	if($sports[1]=='4441') { $ls='baseball'; } 
	if($sports[1]=='4442') { $ls='softball'; } 
}
?>
<form id="customer_list" name="customer_list" method="post" action="">
<input type="hidden" name="HdnPage" id="HdnPage" value="<?php echo $Page; ?>">
<input type="hidden" name="HdnMode" id="HdnMode" value="<?php echo $Page; ?>">
<input type="hidden" name="RecordsPerPage" id="RecordsPerPage" value="<?php echo $RecordsPerPage; ?>">
<input type="hidden" name="hnd_team_id" id="hnd_team_id" value="<?php echo $searchname;?>">
<table class="table table-striped table-hover dataTable no-footer dataTable" id="divisionlistingtable">
<thead>
	<tr>
		<th> Division Id </th>
        <th> Division Name </th>
        <th> Actions </th>
	</tr>
</thead>
<tbody>
<?php

if(isset($_REQUEST['searchbyorganization']))
{
	$OrganizationName     =  $_REQUEST['searchbyorganization'];

	$res = '';
	if($OrganizationName != ''){	
		$res = "select * from customer_division where name like '%$OrganizationName%' and custid in ($ids)";
	} else {
		// $res = "select * from customer_info where user_type='user'";
		$res = "select * from customer_division where custid in ($ids)";
	}
    
    $getResQry      =   $conn->prepare($res);
    $QryArr = array(":organizationame"=>$OrganizationName, ":custid"=>"$customer_id");
    $getResQry->execute($QryArr);
    $getResCnt      =   $getResQry->rowCount();
    $getResQry->closeCursor();
    	$TotalPages = '';
	    if($getResCnt>0){
	        $TotalPages=ceil($getResCnt/$RecordsPerPage);
	        $Start=($Page-1)*$RecordsPerPage;
	        $sno=$Start+1;	           
	        $res.=" limit $Start,$RecordsPerPage";     
	        $getResQry      =   $conn->prepare($res);
	        $getResQry->execute($QryArr);
	        $getResCnt      =   $getResQry->rowCount();
	    if($getResCnt>0){
	        $getResRows     =   $getResQry->fetchAll();
	        $getResQry->closeCursor();
	        $s=1;
	        // print_r($getResRows );
        foreach($getResRows as $division){
			$sportname=$sportname?$sportname:$ls;
		?>
			<tr>
                <td><?php echo $division['id']; ?></td>
                <td nowrap><?php echo $division['name'] ?></td>
				<td><!-- <table><tr><td> -->
												<?php 
												$division_id=$division['id'];
												$division_cust_id=$division['custid'];
												$DivisionQry = $conn->prepare("select * from customer_division where id='$division_id' and custid='$division_cust_id'");
												$DivisionQry->execute(); 
												$CntDivision = $DivisionQry->rowCount();
											    $DivisionResRow = $DivisionQry->fetchAll(PDO::FETCH_ASSOC);
													
												foreach($DivisionResRow as $DivisionRes) {
													
													$customer_division_name =  explode(" - ",$DivisionRes['name']);	$db_division_name =  $customer_division_name[0];
													$db_division_rule =  $customer_division_name[1];
												}
											   
											   ?>
											   <a href="#" id="edit_division" data-id="<?php echo  $division['id'];?>" data-name="<?php echo $db_division_name ?>"
											   data-role="<?php echo $db_division_rule;?>"  data-sports="<?php echo $sportname;?>" data-toggle="modal" class="btn btn-xs btn-success edit_popup"  customerid="<?php echo $division['id']; ?>"><i class="fa fa-pencil"></i> Edit
												</a>   
															
								<!-- </td>
								               
								                        </tr>
								                    </table> -->
                </td>
            </tr>

		<?php
		$s++;
		}
	} 
    else{
           echo "<tr><td colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
       }
    }
     else{
       echo "<tr><td  colspan='3' style='text-align:center;'>No Divisions found.</td></tr>";
     }
	
}?>
 </tbody>
 </table>
<?php
	if($TotalPages > 1){

	echo "<tr><td style='text-align:center;' colspan='3' valign='middle' class='pagination'>";
	$FormName = "customer_list";
	require_once ("paging.php");
	echo "</td></tr>";

	}
?>
</form>

<script src="assets/custom/js/managedivision.js" ></script>



