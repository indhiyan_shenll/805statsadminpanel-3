<?php 
include_once('connect.php');

if (isset($_POST['divisionid'])) {
    $divisionid		= $_POST['divisionid'];
	$conferenceid	= $_POST['conferenceid'];
	$seasonid	    = $_POST['seasionid'];

    $delconfqry = $conn->prepare("delete from customer_conference_division where division_id=:division_id and conference_id=:conference_id");
	$QryArr			= array(":division_id"=>$divisionid,":conference_id"=>$conferenceid);

    $delconfqry->execute($QryArr);

	$delteamqry = $conn->prepare("delete from customer_division_team where division_id=:division_id and conference_id=:conference_id and season_id=:season_id");
	$QryArr			= array(":division_id"=>$divisionid,":conference_id"=>$conferenceid,":season_id"=>$seasonid);

    $delteamqry->execute($QryArr);


	$delplayerqry = $conn->prepare("delete from customer_team_player where division_id=:division_id and conference_id=:conference_id and season_id=:season_id");
	$QryArr			= array(":division_id"=>$divisionid,":conference_id"=>$conferenceid,":season_id"=>$seasonid);

    $delplayerqry->execute($QryArr);

	echo "success";
	exit;
}
