<?php
include_once('session_check.php');
include_once('connect.php');

if (isset($_REQUEST['gameid'])) {

	$gamecode = $_REQUEST["gameid"];
	$season = $_REQUEST["season"];
	$date = explode("-", $_REQUEST["date"]);
	$year = $date[0];
	$actualPlayerid = $_REQUEST["playerid"];
	$dupPlayerid = $_REQUEST["duplicateplayerid"];
	$customer_id = $_SESSION["loginid"];

	$teamCodeQry = $conn->prepare("select * from player_info where id in (:actualPlayerid, :dupPlayerid) order by id asc");
	$teamCodeQryArr = array(":actualPlayerid"=>$actualPlayerid, ":dupPlayerid"=>$dupPlayerid);
	$teamCodeQry->execute($teamCodeQryArr);
	$cntTeamcodeRows = $teamCodeQry->rowCount();
	$returnStatus = "";
	if ($cntTeamcodeRows > 0) {
		$fetchTeamcodePlayers = $teamCodeQry->fetchAll(PDO::FETCH_ASSOC);
		$teamcodeArr = "";
		foreach ($fetchTeamcodePlayers as $fetchTeamcodePlayersRows) {
			$teamcodeArr[$fetchTeamcodePlayersRows["id"]]["teamcode"] = $fetchTeamcodePlayersRows["team_id"];
			$teamcodeArr[$fetchTeamcodePlayersRows["id"]]["actcheckname"] = strtoupper($fetchTeamcodePlayersRows["lastname"].", ".$fetchTeamcodePlayersRows["firstname"]);
			$teamcodeArr[$fetchTeamcodePlayersRows["id"]]["dupcheckname"] = strtoupper($fetchTeamcodePlayersRows["lastname"].", ".$fetchTeamcodePlayersRows["firstname"]);
		}
		if (count($teamcodeArr) == 2) {
			if (count(array_unique($teamcodeArr)) === 1) {
				$teamcode = $teamcodeArr[$actualPlayerid]["teamcode"];
				$checkname = $teamcodeArr[$actualPlayerid]["actcheckname"];
				$statsQry = $conn->prepare("select * from individual_player_stats where playercode in (:actualPlayerid, :dupPlayerid) and gamecode=:gamecode and teamcode=:teamcode");
				$statsQryArr = array(":actualPlayerid"=>$actualPlayerid, ":dupPlayerid"=>$dupPlayerid, ":gamecode"=>$gamecode, ":teamcode"=>$teamcode);
				$statsQry->execute($statsQryArr);
				$cntRows = $statsQry->rowCount();
				if ($cntRows > 0) {

					//Update duplicate playerID into actual playerID
					$updatePlayer = $conn->prepare("update individual_player_stats set playercode=:actualPlayerid, checkname=:checkname where playercode=:dupPlayerid");
					$updatePlayerArr = array(":actualPlayerid"=>$actualPlayerid, ":dupPlayerid"=>$dupPlayerid, ":checkname"=>$checkname);
					$updatePlayer->execute($updatePlayerArr);
					
					$sumPlayerstats = $conn->prepare("SELECT SUM(gp) as gp,SUM(gs) as gs,SUM(fgm) as fgm,SUM(fga) as fga,SUM(fgm3) as fgm3,SUM(fga3) as fga3, SUM(ftm) as ftm,SUM(fta) as fta,SUM(tp) as tp,SUM(oreb) as oreb,SUM(dreb) as dreb,SUM(treb) as treb, 
			        SUM(pf) as pf,SUM(tf) as tf,SUM(ast) as ast,SUM(to1) as to1, SUM(blk) as blk,SUM(stl) as stl,SUM(min) as min
			        FROM individual_player_stats where playercode = :actualPlayerid and teamcode=:teamcode and season=:season");	
			        $sumPlayerstatsArr = array(":actualPlayerid"=>$actualPlayerid, ":season"=>$season, ":teamcode"=>$teamcode);
			        $sumPlayerstats->execute($sumPlayerstatsArr);
			        $cntPlayerStatsRows = $sumPlayerstats->rowCount();
			        if ($cntPlayerStatsRows > 0) {
			        	$fetchdata = $sumPlayerstats->fetch(PDO::FETCH_ASSOC);
			            $gp = $fetchdata["gp"];
			            $gs = $fetchdata["gs"];
			            $fgm = $fetchdata["fgm"];
			            $fga = $fetchdata["fga"];
			            $fgm3 = $fetchdata["fgm3"];
			            $fga3 = $fetchdata["fga3"];
			            $ftm = $fetchdata["ftm"];
			            $fta = $fetchdata["fta"];
			            $tp = $fetchdata["tp"];
			            $oreb = $fetchdata["oreb"];
			            $dreb = $fetchdata["dreb"];
			            $treb = $fetchdata["treb"];
			            $pf = $fetchdata["pf"];
			            $tf = $fetchdata["tf"];
			            $ast = $fetchdata["ast"];
			            $to1 = $fetchdata["to1"];
			            $blk = $fetchdata["blk"];
			            $stl = $fetchdata["stl"];
			            $min = $fetchdata["min"];

			            $playerStatsbb = $conn->prepare("SELECT * FROM player_stats_bb where playercode = :actualPlayerid and season=:season and teamcode=:teamcode");
			            $playerStatsbbArr = array(":actualPlayerid"=>$actualPlayerid, ":season"=>$season, ":teamcode"=>$teamcode);
			            $playerStatsbb->execute($playerStatsbbArr);
			        	$cntPlayerStatsbbRows = $playerStatsbb->rowCount();
			            if ($cntPlayerStatsbbRows > 0) {

			                //Update player_stats_bb
			            	$updatePlayerStatsbb = $conn->prepare("update player_stats_bb set gp=:gp, gs=:gs, fgm=:fgm, fga=:fga, fgm3=:fgm3, fga3=:fga3, ftm=:ftm, fta=:fta, tp=:tp, oreb=:oreb, dreb=:dreb, treb=:treb, pf=:pf, tf=:tf, ast=:ast, to1=:to1, blk=:blk, stl=:stl, min=:min where playercode=:actualPlayerid and teamcode=:teamcode and season=:season ");
			            	$updatePlayerStatsbbArr = array(":gp"=>$gp, ":gs"=>$gs, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":ast"=>$ast, ":to1"=>$to1, ":blk"=>$blk, ":stl"=>$stl, ":min"=>$min, ":season"=>$season, ":teamcode"=>$teamcode, ":actualPlayerid"=>$actualPlayerid);
			                $updatePlayerStatsbb->execute($updatePlayerStatsbbArr); 

			            } else {

			            	//Insert player_stats_bb
			                $insertPlayerStatsbb = $conn->prepare("insert into player_stats_bb (checkname,customer_id,gp,gs,fgm,fga,fgm3,fga3,ftm,fta,tp,oreb,dreb,treb,pf,tf,ast,to1,blk,stl,min,playercode,year,season,teamcode) 
			                values (:checkname, :customer_id, :gp, :gs, :fgm, :fga, :fgm3, :fga3, :ftm, :fta, :tp, :oreb, :dreb, :treb, :pf, :tf, :ast, :to1, :blk, :stl, :min, :actualPlayerid, :year, :season, :teamcode)");
			                $insertPlayerStatsbbArr = array(":gp"=>$gp, ":gs"=>$gs, ":fgm"=>$fgm, ":fga"=>$fga, ":fgm3"=>$fgm3, ":fga3"=>$fga3, ":ftm"=>$ftm, ":fta"=>$fta, ":tp"=>$tp, ":oreb"=>$oreb, ":dreb"=>$dreb, ":treb"=>$treb, ":pf"=>$pf, ":tf"=>$tf, ":ast"=>$ast, ":to1"=>$to1, ":blk"=>$blk, ":stl"=>$stl, ":min"=>$min, ":season"=>$season, ":teamcode"=>$teamcode, ":year"=>$year, ":actualPlayerid"=>$actualPlayerid, ":customer_id"=>$customer_id, ":checkname"=>$checkname);
			                $test = $insertPlayerStatsbb->execute($insertPlayerStatsbbArr);
			            } 

			            // Delete duplicate player from player_stats_bb
			            $deletePlayerinfo = $conn->prepare("delete from player_stats_bb where playercode=:dupPlayerid ");
			            $deletePlayerinfoArr = array(":dupPlayerid"=>$dupPlayerid);
			        	$deletePlayerinfo->execute($deletePlayerinfoArr);			        	

			            // Delete duplicate player from player_info
			            $deletePlayerinfo = $conn->prepare("delete from player_info where id=:dupPlayerid");
			            $deletePlayerinfoArr = array(":dupPlayerid"=>$dupPlayerid);
			        	$deletePlayerinfo->execute($deletePlayerinfoArr);

			        	$returnStatus["status"] = "Player combined and duplicate player deleted successfully";
			        	$returnStatus["responsestatus"] = "success";

			        }		

				} else {
					$returnStatus["status"] = "No entries found in stats table for both players ";
					$returnStatus["responsestatus"] = "failure";
				}

			} else {
				$returnStatus["status"] = "Actual player/Duplicate player in different teams";
				$returnStatus["responsestatus"] = "failure";
			}
		}

	} else {
		$returnStatus["status"] = "Both players availble in player_info";
		$returnStatus["responsestatus"] = "failure";
	}
	echo json_encode($returnStatus);    
}
exit;
?>