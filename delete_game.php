<?php
include_once('session_check.php'); 
include_once("connect.php");
include_once('usertype_check.php');
error_reporting(E_ALL);
if (isset($_GET['gid'])) {

	if ($_GET['action'] == "delete") {
		
		$gid = $_GET['gid'];
		$sportname = $_GET['sportname'];
		$DeleteQry = $conn->prepare("delete from games_info where id=:gid");
		$DeleteQryArr = array(":gid"=>$gid);
		$DeleteStatus = $DeleteQry->execute($DeleteQryArr);
		if (empty($sportname)) {
			header('Location:game_list.php?msg=4');
	        exit;
	    } else {
	    	header('Location:game_list.php?msg=4&sport='.$sportname);
		    exit;
	    }
		
	}
}
?>